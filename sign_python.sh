#!/bin/bash
#Author: Alexander Hackett 
#Student Number  15323791 
#ahackett@tcd.ie 
#Created for Prof. Archers assignment, Ising Model Simulation
#Author: Alexander Hackett 
#Student Number  15323791 
#ahackett@tcd.ie 
#Created for Prof. Archers assignment, Ising Model Simulation

#This simple bash script adds a short 'signature', including my name, student no and email to every python file and bash script in the directory,
#I will use this to sign the python files once they are all complete
#Update, the signing script worked, it even signed itself!

#Sign all the python files, using ''', python block comment
for pythonfile in *.py
do
    sed -i "1 a ''' \n Author: Alexander Hackett \n Student Number  15323791 \n ahackett@tcd.ie \n Created for Prof. Archers assignment, Ising Model Simulation \n'''" $pythonfile
done

#Now, sign all the shell scripts, using # at the start of the line as a comment
for bashfile in *.sh
do
    sed -i "1 a #Author: Alexander Hackett \n#Student Number  15323791 \n#ahackett@tcd.ie \n#Created for Prof. Archers assignment, Ising Model Simulation" $bashfile
done
