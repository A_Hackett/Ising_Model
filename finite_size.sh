#!/bin/bash
#Author: Alexander Hackett 
#Student Number  15323791 
#ahackett@tcd.ie 
#Created for Prof. Archers assignment, Ising Model Simulation

#Author: Alexander Hackett,
#15323791
#ahackett@tcd.ie
#Created for Prof. Archer's Assignment, Ising Model Simulation

#This shell script runs the Measure_2D_Ising_Model.py script numerous times with a differing command line argument
#Each time in order to generate a series of appropriately named ASCII files that can
#Be read by peak_getter.py in order to determine the critical temperature of the Ising Lattice 
#Of an infinite size via linear regression

#These are the sizes of arrays we will try out
sizes=(10 15 20 25 30 35 40 45 50 60 70)
#Run the Python script with each size as a CL argument
for i in "${sizes[@]}"
do
	python Measure_2D_Ising_Model.py $i
done
#Run the peak_getter.py in order to determine the critical temperature via linear regression
python peak_getter.py

#EOF
