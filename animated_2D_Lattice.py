#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This python script is to be invoked by the shell script 'animated2D.sh'
This script will thermalize the 2D Ising Model at a given temperature and size, 
and thermalized for a given number of 'sweeps' (1 sweep = NxIterations, where N = size^2)
And will save the output configurations as png files, which will be converted 
to an animated .gif by the shell wrapped in order to be included into the
final repo README
The commands to be passed are, temperature, size, iterations
'''

import Ising_Simulation_Package as IS
import matplotlib as mpl
mpl.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import math
import sys
import os



def main():
    
    #Read the command line arguments passed by the shell script
    try:
        temperature =float(sys.argv[1])
    except IndexError:
        temperature = 2.269 #Set the default temperature at the critical temperature
    try:
        size = int(sys.argv[2])
    except IndexError:
        size = 50 #Nice Large default size
        
    try:
        iterations = int(sys.argv[3])
    except IndexError:
        iterations = 800 #Large number of default iterations
    #Error handling will be done by the shell script, since this script
    #Is not designed to be run alone
    
    #make a lattice instance
    vis_lattice = IS.ising_lattices(size, temperature, 0, 'ups')
    #Thermalize the lattice in a loop, saving a png every iterations
    for i in range(iterations):
        vis_lattice.rand_metro_relax(size**2) #Sweep, on average, each atom
        #Create a file names
        it_name = str(i)
        #Ensure filenames are padded to 5 zeros if needed
        filename = 'Ising_Animation_Files/Ising_Visualization0' + it_name.rjust(4, '0') + '.png'
        #Convert the path to a windows path if needed (changes forward slashes to backslashes)
        filename = os.path.normcase(filename)
        #Plot the current configuration
        plt.imshow(vis_lattice.lattice_values)
        #Add a title
        plt.title('Visualization of the Ising Model')
        #Save the png
        plt.savefig(filename)
        #and close the png
        plt.close()
        
    print('Files Sucessfully Written!')
    
if __name__ == '__main__':
    main()




