#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''
'''
This script loads in the first N towns, based on a CL argument in Ireland, in alphabetical order,
and based on Euclidian distance, using the Simulated Annealing Heuristic in order
to determine a 'good' route between them as per the TSP.
The Initial Greedy algorithm route is visualized, and compared to the (hopefully)
superior annealed route
The sample of all towns in Ireland beginning with A was downloaded for free in
CSV format from https://www.irelandtownslist.com
'''
#Import the two dependancies
import Simulated_Annealing_Package as SAP
import anneal_vis as AV
#We also need a CSV reader, since we'll be parsing that database
import numpy as np #Numpy has a csv reader!
#And we need to parse command line arguments,
import sys


#Parse the command line argument
number_of_towns = int(sys.argv[1]) #Since sys.argv[0] is always the script name
#we need at least 4 towns to have a route we can flip so
if number_of_towns < 4:
    raise RuntimeError('Not Enough Towns! Please Pass an Integer Greater than 4 Through the CLI')

'''
Importing the data from the CSV file
'''
#TODO, Make it so the script wgets the file from my webserver (Since it's behind a
#download link on the website) if it's not in the working directory!
#Might be best to do this in a bash script outside? DONE! In external bash script

'''
Reading in the data to generate coordinates
'''
#Parse the data into a var with numpy
town_data = np.genfromtxt('ie-towns-sample.csv',delimiter=',')
#latitude is in column 9, and longtitude in column 10
#I always think of latitude as being up and down, and longtitude as being across
#So we make lat our y coord and long our x coord
#Take data for as many towns as the command line argument specified
#Strip out the headers! Luckly, np.genfromtxt reads these in as NaNs so:
lat = town_data[:number_of_towns,9][~np.isnan(town_data[:number_of_towns,9])] 
lon = town_data[:number_of_towns,10][~np.isnan(town_data[:number_of_towns,10])]
#Can't use our own helper function here, since we have two lists of coordinates, rather than
#one list of points
#USe np.column_stack for the desired effect
town_coords = np.column_stack((lon,lat))


'''
Perform the Simulated Annealing
'''
irish_towns_tsp = SAP.Salesman_Problem(town_coords)#Try using the defaults for now
#Anneal a solution!
irish_towns_tsp.simulated_anneal() #Again, with the defaults


'''
Visualize the solutions!
'''
irish_towns_tsp_vis = AV.vis_anneal(town_coords,irish_towns_tsp.first_route,
                                    irish_towns_tsp.first_route_length,
                                    irish_towns_tsp.shortest_route,
                                    irish_towns_tsp.shortest_length)
irish_towns_tsp_vis.set_xy_name('Longtitude','Latitude')
irish_towns_tsp_vis.set_units('Degrees')
irish_towns_tsp_vis.vis_data()


