#!/usr/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''


'''
----------------------------------------------------------------------------
This is the main Ising Model script
Alexander Hackett
15323791
02/11/17
----------------------------------------------------------------------------
'''

import os
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import numpy as np
from random import randrange, random, choice
import time
import os.path  

#Defining a class of all ising lattices
#the square lattices have a size, a constant temperature and a magnetic field
class ising_lattices:
    def __init__(self, size, temp, field, init_spins):
        self.size = size #side lenght of the array in atoms
        self.temp = temp #In normalized units
        self.field = field #Magnetic field in normalized units
        self.init_spins = init_spins #So we can track the initial configuration
        if self.init_spins == 'random': #If the initial configuration is set to randomize
            self.lattice_values = self.mk_rand() #Use the method to make a random lattice
        elif self.init_spins == 'ups':
            self.lattice_values = self.mk_up() #Otherwise, make an aligned lattice
        self.curr_mag = np.float64(0) #This property will store the current 
        #running total magnetization
        #This variable will store the current running total energy
        self.curr_energy = np.float64(0)
        self.det_energy() #Initialize the energy
        self.det_mag() #Initialize the magnetization
        self.t_ave_e = np.float64(0) #Initialize the average energy
        self.t_ave_m = np.float64(0) #And average magnetization
        self.mlist = [] #This list will track the changes to magnetization as
        #we update the lattice via metropolis algorithm
        #This list will track the changes to energy as we update the lattice
        #via metropolis algorithm
        self.elist = []
        self.mag_sus = np.float64(0) #Initilize the magnetic susceptabilit
        self.heat_cap = np.float64(0) #And the heat capacity
        
        
    def det_energy(self):
        '''
        This method measures the total energy of the lattice in any particular
        configuration, and stores it in self.curr_energy. The value is
        divided by 2 to avoid double counting, but is not normalized to
        be per atom. This must be done later
        '''
        energy = np.float64(0) #Start counting the energy
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                energy = energy + self.E_xy(x, y) #Determine the energy at each spin
        self.curr_energy = energy / 2. #To prevent double counting in the long run
        
    def det_mag(self): #Determines the magnitization of the lattice in its current config
        '''
        Analagous to det_energy(), this method determines the total magnetization
        of the lattice in its current configuration, and stores it in
        self.curr_mag. This value is not normalized to be per spin
        nor is it the absolute value, ie it can be negative
        '''
        mag = np.float64(0) #Start counting the magnitization
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                mag = mag + self.lattice_values[x, y] #Add +1 for an up spin
                #And -1 for a down spin, determining magnitization
        self.curr_mag = mag 
        
    def set_temp(self, new_temp):
        '''
        This method sets the current normalized temperature of the lattice,
        used by the two metropolis algorithm methods to a new float value,
        new_temp
        '''
        self.temp = new_temp
        
    def mk_rand(self): #Make an array of random spins
        '''
        This method produces a the square Ising lattice with randomly selected
        spins, representative of a high temperature state to be used to
        initlize the ising lattice
        '''
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = choice([1, -1]) #Randomly assign spins
        return lat
    
    
    def mk_up(self): #Make an array of up spins
        '''
        This method produces a the square Ising lattice with all
        spins aligned up, representative of a zero temperature state to be used
        to initlize the ising lattice
        '''
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = 1 #set all spins to 1
        return lat
    
    
    def E_xy(self, x, y): #Compute the energy of some lattice point, using ising model hamiltonian
        '''
        This method determines the energy of a particular spin in the two D
        Ising lattice, based on the nearest neighbor Hamiltonian and 
        Periodic boundary conditions
        '''
        if self.field==0: #If there's no magnetic field present energy depends on nearest only
            #Using periodic boundry conditions, will swap to supercell soon
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]))
        else: #If there is a magnetic field, energy will also depend on field strenght
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]) +
                    self.lattice_values[x, y]*self.field)

    def rand_metro_relax(self, nsteps): 
        '''
        #A metropolis alogorithm, randomly selecting atoms to be flipped 
        according to selection rules, if done for enough iterations, this should
        thermalize the lattice properly, hence the function name
        '''
        for i in range(int(nsteps)): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
        self.det_mag() #Update the magnitization of this relaxed configuration
        self.det_energy() #Update to the energy of this relaxed configuration
    def rand_metro_measure(self, nsteps):
        '''
        This is a randomly selecting metropolis algorithm designed to measure
        4 vital characteristics of a thermalized lattice, (all thermal averages):
        The magnitization per spin, the energy per spin, the magnetic susceptibility 
        per spin and the heat capacity per spin
        Make sure to thermalize your lattice with rand_metro_relax() before
        Using this!
        '''
     
        #For now, they contain the values at initial thermalization!
        #These would have been updated after thermalization, or on
        #Initialization if you havn't bothered to thermalize!!
        for i in range(int(nsteps)): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            flip = False #We haven't flipped this one yet
            #check its spin
            #old_spin = self.lattice_values[x, y]
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                flip = True #We flipped it
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                flip = True #We flipped it
            if flip: #If we flipped, need to update energies and mags
                self.curr_energy += E_delta / 2. #To prevent double counting in the long run
                self.curr_mag += self.lattice_values[x, y]
                self.mlist.append(self.curr_mag)
                self.elist.append(self.curr_energy)
        self.t_ave_e = self.curr_energy / self.size**2
        self.t_ave_m = self.curr_mag / self.size**2
        ave_e = np.average(self.elist)
        ave_m = np.average(self.mlist)
        ave_e_sq = ave_e**2
        ave_m_sq = ave_m**2
        try:
            ave_m_to_be_squared = sum(np.array(self.mlist)**2)/len(self.mlist)
        except ZeroDivisionError:
            ave_m_to_be_squared = np.float64(0)
        try:
            ave_e_to_be_squared = sum(np.array(self.elist)**2)/len(self.elist)
        except ZeroDivisionError:
            ave_e_to_be_squared = np.float64(0)
        self.mag_sus = (self.size**(-2)*(1.0/self.temp))*(ave_m_to_be_squared-ave_m_sq)
        self.heat_cap = (self.size**(-2)*(1.0/self.temp)**2)*(ave_e_to_be_squared-ave_e_sq)
        
         
    '''
    #THESE DON'T WORK AS OF NOW
    def rand_metro_ann(self, nsteps, Ts): #A metropolis alogorithm, randomly selecting atoms
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(Ts)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
    def raster_metro(self, nsteps): #A metropolis alogorithm, scanning over each atom in order
        z = 0
        while z < nsteps:
            for i in range(self.size): #Do this for as many iterations as requested
                x = i #Loop over atoms in order
                for j in range(self.size):
                    y = j
#Work out the change in energy for flipping the atom's spin, new energy minus old energy
                    E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
                    #Checking if the energy is tending to a minimum
                    if E_delta <= 0.: #Is the energy change negative?
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Does a boltzmann velocity distro allow the flip anyway?
                    elif random() < np.exp(-1. * E_delta/(self.temp)):
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Otherwise, keep the spin the same
            z += 1
    '''
    
def Ising_Model_Simulation(temps, iterations, meaning_iterations, size):
    '''
    This helper function allows the two dimensional ising model to be more 
    easily run over a range of temperatures and meaning iterations, gathering
    measurments of the four thermodynamic quantities
    '''
    mean_energy = [] #This list will store the meaned energies
    mean_magnet = [] #This list will store the mean magnetizations
    mean_heat_cap = [] #Same, for heat capacitites
    mean_magnetic_sus = [] #Same for magnetic susceptability
    for j in range(meaning_iterations):
        '''
        Run the entire temperature range simulation for as many times as
        is desired in order to achieve a better statistical average
        '''
        temps = temps
        #ensure the # of thermalizing iterations is an integer
        iterations = round(iterations) 
        #Measure for half that number of iterations
        measure_iterations = round(iterations/2)
        ave_magnetizations = [] #List to store the magnetization for this run
        ave_energies = [] #And the energies
        magnetic_susceptibilities = [] #magnetic suscepts
        heat_capacities = [] #Heat capacitites
        #Create an Ising lattice at the initialy requested temperature
        #and size, no field at an aligned, low temperature config
        lattice1 = ising_lattices(size, temps[0], 0, 'ups')
        for i in temps: #for each temperature
            '''
            Run the metropolis algorithms to thermalize and measure
            the lattice at each temperature
            '''
            lattice1.set_temp(i) #heat the lattice to the new temperature
            lattice1.rand_metro_relax(iterations) #Thermalize it
            lattice1.rand_metro_measure(measure_iterations) #measure it
           
            '''
            Magnetization = <M>/#spins
            energy = <E>/spins
            heat capacities = 1/T^2 <E^2> - <E>^2
            susceptibilities = 1/T <M^2> - <M>^2
            '''
            ave_energies.append((lattice1.t_ave_e))
            ave_magnetizations.append(abs(lattice1.t_ave_m))
            heat_capacities.append(lattice1.heat_cap)
            magnetic_susceptibilities.append(lattice1.mag_sus)
            #print('Simulation For Temperature ',i,' Complete') #Debugging
        #Append all the measurements to the meaning lists
        mean_energy.append(ave_energies)
        mean_magnet.append(ave_magnetizations)
        mean_heat_cap.append(heat_capacities)
        mean_magnetic_sus.append(magnetic_susceptibilities)
        #debugging/benchmarking
        print('Simulation Complete for Iteration', j,' of ',meaning_iterations)
    #Average the measurments at each temperature
    mean_energy = np.array(np.mean(mean_energy, axis = 0))
    mean_magnet = np.array(np.mean(mean_magnet, axis = 0))
    mean_heat_cap = np.array(np.mean(mean_heat_cap, axis = 0))
    mean_magnetic_sus = np.array(np.mean(mean_magnetic_sus, axis = 0))
    #Return these lists as the output of the function
    return temps, mean_energy, mean_magnet, mean_heat_cap, mean_magnetic_sus


def main():
    '''
    A test set of measurements wrapped in main
    '''
    temps = np.linspace(1.5,3.5, 50)
    temps, mean_energy, mean_magnet, mean_heat_cap, mean_magnetic_sus = Ising_Model_Simulation(temps, 1e5, 5, 20)
    #Writing the output to disk
    filename = 'name=main_sample_programme.txt'
    print('Writing ASCII File')
    lattice_data = Table([temps,mean_energy,mean_magnet,mean_heat_cap,mean_magnetic_sus], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
    ascii.write(lattice_data, filename)
    #self emailing details
    my_email = 'alexjanhackett@gmail.com'
    my_passw = '' #left out for security rn
    recipients = ['alexjanhackett@gmail.com']
    subject = 'Ising_Package name = main'
    message = 'This is the output of a 20x20 lattice, thermalized 1e5 iterations, measured 5e4 iterations, each meaned 5 times, over a temperature range from 1.5 to 3.5 with 50 steps. I am reciving this email and file because I ran Ising_Simulation_Package.py as a programme rather than importing it! Hopefully the data is still of some use!! '
    file_name = filename
    #Emailing the disk file to myself!
    emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)

if __name__ == '__main__':
    main()

