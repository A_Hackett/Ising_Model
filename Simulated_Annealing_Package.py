#!/bin/env python

''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This module provides a class and helper functions for annealing a solution
to the traveling salesman problem
'''
#Importing needed packages
import numpy as np
from random import randrange, random, choice, randint, shuffle, sample

  
'''
HELPER FUNCTIONS
'''
def initial_route_key(x):
    '''
    A helper function for the Greedy algorithm, to be
    used as a key in min(), to retrieve the closest city, rather than 
    the distance to that city
    edit: After some googling I determined that lambda could be used instead of
    this for a key
    '''
    #TODO, replace with with a lambda thing in make_inital_route()
    return x[0]

def convert_coords(x = [], number = 100, sample_space = 1000):
    '''
    A helper function for inputting the city coordinates in the right form
    takes an array of tuples as an input, and produces an output that can be
    fed into the main class. The helper function can also be used to generate
    a random set of coordinates for trialing the program. random.sample provides its
    own error handling if the user tries to generate a number of cities greater than
    the sample space
    '''
    if not x: #If no points were provided, generate some
        tuple_space = sample(range(sample_space**2), number)
        x = [divmod(i, sample_space) for i in tuple_space]
    if len(x) > 2: #We need at least 2 cities to make a route (albit a dull one)
        return np.row_stack(x) #Rowstack the tuples so they can be used as coordinates
    else: #Warn the user that they're input/generated an illegal # of cities
        raise ValueError('You Must Provide/Generate At Least 2 Cities!')
    



class Salesman_Problem:
    '''
    A class for solving the Travelling Salesman Problem!
    Instances required a set of coordinates, representing all the cities that
    must be travelled to, a starting formal temperature parameter, and, optionally,
    if the initial starting route should be chosen randomly, or, as is default, 
    by the Greedy algorithm
    '''
    def __init__(self, city_coords, starting_temperature = 20, random_start = False):
        '''
        The Initialization Method, called upon the creation of an instance of
        Salesman_Problem(). It initializes the input coordinates of the cities. 
        It creates a distance matrix from the city coordinates. It sets the initial 
        formal temperature paramter of the problem, and intialzes the properties that
        track the inital the best solution to the TSP
        '''
        self.city_coords = city_coords #These are the coordinates to be used
        #The coordinates must be in the form output by np.column_stack, an 
        #array, containing an array for each city, with each cities array containing
        #two elements, one x coordinate and one y coordinate
        #Alternatively, utilize the helper function convert_coords() to generate
        #These from a list of points, where an array is input [(x1,y1), (x2,y2) etc]
        self.numb_cities = len(city_coords) #This is just the number of cities we have
        self.temp = starting_temperature #And the initial formal temperature paramter
        self.distances = [] #This matrix will hold the matrix of distances from each
        #city to every other city
        self.get_distances() #Populate the distance matrix
        self.curr_route = [] #This will hold our current route
        if not random_start:#If a random start wasn't specified
            self.make_initial_route() #Generate a route according to the Greedy Algorithm
        else: #otherwise, pick a totally random route
            self.make_random_route()
            
        #This is the length of our first route
        self.curr_length = self.E_route(self.curr_route)
        self.first_route = self.curr_route #The current route after initializing is the first route
        self.first_route_length = self.curr_length #And its length is the first length
        self.shortest_route = self.curr_route#It's also our shortes(only) route, for the time being
        self.shortest_length = self.curr_length#And its length
        
    def set_temp(self, new_temp):
        '''
        This method sets the formal temperature parameter of 
        the travelling salesman problem to a new desired temperature
        '''
        self.temp = new_temp
        
    def get_distances(self):
        '''
        This method generates a matrix of distances from the coordinates of the 
        cities given as input, to allow easy comparisons to be made
        '''
        #Get Euclidian distances (Not ideal for latitudes and longtitudes)
        #But fine as a proof of concept
        #Subtract the squares of all the coordinates. None and np.newaxis 
        #do the same thing, they turn the arrays of coordinates into a matrix of arrays,
        #each array in the matrix gives the distance from city n to all the other cities
        #so distances[0][0] gives 0 and [1][1] gives zero etc
        #Then takes the square root of each of these, so the actual distances,
        #not the distances squared are being stored
        self.distances = np.sqrt((np.square(self.city_coords[:, None] -
                                                 self.city_coords).sum(axis=2)))  
    def make_random_route(self):
        '''
        This method makes the second simplest path between cities I could think of, 
        just picks some random route
        '''
        route = [i for i in range(self.numb_cities)] #List the cities in order of input
        shuffle(route) #Randomize the list
        self.curr_route = route  #Hey presto, there's your route
        
    def make_input_route(self):
        '''
        Makes the simplest route I could think of, just visit every city in the 
        order it was input into the instance
        '''
        route = [i for i in range(self.numb_cities)]
        self.curr_route = route
        
   
        
        
    def make_initial_route(self):
        '''
        This method creates an intial 'pretty good' route for the
        travelling saleman problem by using the 'Greedy Algorithm', it starts 
        at some random city, and just always goes to the closest unvisted city
        '''
        city = randrange(self.numb_cities) #Pick a random city
        route = [city] #This will be the starting point on the route
        remaining_cities = list(range(self.numb_cities)) #make a list of all the cities we have
        remaining_cities.remove(city) #Remove the one we just started from

        #The greedy algorithm itself
        while remaining_cities != []: #While we have cities left to visit
            #Work out the closest city to the last city we were in, by using
            #the distance matrix. Take the distance between the current city and all the
            #other remaining cities, then use the key helper function to take the 
            #distance value out. Then use min to determine the order of the cities
            #in terms of distance            
            closest_city = min([(self.distances[city][i], i) for i in remaining_cities], key=initial_route_key)
            #The closest city is always just staying at the city we're already at,
            #So take the second city on the list, which is the real closest city
            city = closest_city[1]
            #Pop that city off the stack, so we don't visit it again
            remaining_cities.remove(city)
            #And add the city to the route
            route.append(city)

        self.curr_route = route
        
    def E_route(self, route):
        '''
        This method determines the length (Energy) of a given route
        '''
        #Just sum all the distances between each city on the route, and the next city
        #zip the route with the route where the first and last city are swapped, and then iterate over that zip
        #Determining the appropriate distances between each of the cities on the route
        #Finally, sum all the distances, in order to get the full length of the route
        return sum([self.distances[i, j] for i, j in zip(route, route[1:] + [route[0]])])
    
    
    def simulated_anneal(self, temps = np.linspace(20,0.0001,10000), iterations_per_temp = 1000):
        '''
        This is the simulated annealing method! It takes an array of temperatures for
        the annealing to be simulated at, and a number of iterations to perform at
        each temperature, for a better chance of getting a good solution
        '''
        for t in temps: #Do this as we cool, so for each temperature
            self.set_temp(t) #Set the formal temperature parameter to the new temp
            print('Now Annealing in Temperature Block Starting at ',t) #debugging
            #Do the following as many times as desired
            for _ in range(iterations_per_temp): 
                flipped_route = False #We haven't yet altered out current route
                new_route = list(self.curr_route) #Take our current route, as a proper list
                i = randint(2, self.numb_cities - 1) #Make a random integer between 2
                #and the number of cities we have -1
                j = randint(0, self.numb_cities - i)
                #And another random int between 0 and the #of cities - the other random int
                
                #This (hopefully) the algorithm as described in Prof. Hutzler's notes
                
                #Currently the new route is just the current route
                #take a lenght i portition of the route, from city j to 
                #city i + j
                #Reverse the order of the cities in this section of the route
                new_route[j: (j + i)] = reversed(new_route[j: (j + i)])
                #This produces a new route
                new_length = self.E_route(new_route) #Determine the length of the new route
                E_delta = new_length - self.curr_length #How does the length of the
                #new route compare to the length of the old one?
                if E_delta <= 0.: #if the new route is shorter than the old one
                    flipped_route = True #We'll keep the flipped one

                elif random() < np.exp(-E_delta / self.temp): # If Boltzmann allows it anyway
                    flipped_route = True #We'll flip it

                if flipped_route: #If we did end up flipping it
                    self.curr_route = new_route #The new route is now our current route
                    self.curr_length = new_length #And its length is now our current length
                    #Now, check if the new route is the best route we've got so far
                    if new_length < self.shortest_length: #If this is the best route so far
                        self.shortest_length = new_length #Make its length our new shortest length
                        self.shortest_route = new_route #Make it our new shortest route
        
        

'''
#TODO, write a test case!
#Method to write data to file, since I have to be able to plot externally to
#python, limitations of the home server! UNEEDED! RUNS FINE ON SLOWER LAPTOP
#Write a python script to use as a visualizer! DONE!


#TODO, 
#Write get_distances DONE!, a method to turn a list of coordinates into a matrix of distances DONE!
#Write make_initial_route(), a method to get an initial route (Use 'Greedy algorithm as per
#prof Hutzler's notes?') DONE! Using Greedy method
#Think of some way to swap cities in a route to make a new route, as in the notes
#Done! Using 2opt algorithm as detailed in the notes DONE!
'''


#Writing Test Case
def main():
    #Generate some random coordinates, using default values
    random_coordinates = convert_coords()
    #Create the Salesman problem instance, using default inital route
    #The greedy algorithm, and a default starting temperature
    random_test_problem = Salesman_Problem(random_coordinates)
    #Anneal it using default temperatures and default iterations
    random_test_problem.simulated_anneal()
    print('Greedy Distance Between Random Towns: ', random_test_problem.first_route_length,' Units')
    print('Annealed Distance Between Random Towns: ',random_test_problem.shortest_length, ' Units' )
    #Test Case Works!!!
    #TODO, Write a vizualization package, to make plots of the initial and optimal routes!

if __name__ == '__main__':
    main()
    






