#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''
#importing the 2D Ising simulation package
import Ising_Simulation_Package as IS
import numpy as np
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import os.path
import sys

'''
This Version of the 2D Ising Model calling script takes a command line argument,
the nxn size of the lattice, and produces two files per run, each containing the
temperature values, and the values of magnetic suspectability or heat capacity
This was designed to be run with the finite_size.sh script
'''
#wrapping everything in main()
def main():
    size_to_be_run = int(sys.argv[1]) #Parse the command line argument
    
    temp = np.linspace(2,3,20) #Take many temperature points around the critical temp
    #Ten meaning iterations is resonable. Need n^3 thermalizing to get good data
    T, E, M, HOT, SUS, = IS.Ising_Model_Simulation(temp, (size_to_be_run**2)**3, 2, size_to_be_run)
    print('Simulation Complete')
    
    #Create the appropriately named tables
    filename1 = 'heatcapacity' + str(size_to_be_run) +'lattice_for_measurement_of_lattice_size.txt'
    print('Writing ASCII File')
    lattice_data1 = Table([T,HOT])
    ascii.write(lattice_data1, filename1)#Write the table to an ASCII file to be read by the peak_getter.py
    
    #Analagously for the magentic susceptability
    filename2 = 'suscept' + str(size_to_be_run) +'lattice_for_measurement_of_lattice_size.txt'
    print('Writing ASCII File')
    lattice_data2 = Table([T,SUS])
    ascii.write(lattice_data2, filename2)
    
if __name__ == '__main__':
    main()



