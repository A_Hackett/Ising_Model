#!/bin/bash
#Author: Alexander Hackett 
#Student Number  15323791 
#ahackett@tcd.ie 
#Created for Prof. Archers assignment, Ising Model Simulation

#Author: Alexander Hackett,
#15323791
#ahackett@tcd.ie
#Created for Prof. Archer's Assignment, Ising Model Simulation

#This shell script runs the animated 2d python script in order to generate frames
#in order to create a gif, and then deletes those files
#Warning! This Script will alter files and Directories! It won't work in a write protected directory without being sudo

read -p 'What Temperature to Thermalize the Ising Lattice at? ' temp
read -p 'How Large a Lattice to use? [Hit Space for default]' size
read -p 'How Many Sweeps? [Hit Space for Default] ' sweeps


#Make a directory to to fill with files
mkdir Ising_Animation_Files
#Run the script, filling the folder with images
python animated_2D_Lattice.py $temp $size $sweeps
#Move into the new directory
cd Ising_Animation_Files
#Use imagemagick, to make a gif
convert -loop 0 -delay 5 *.png Ising_Animation.gif
#Delete the temp png files (We made a new directory to ensure that all the users other png files are safe)
rm *.png

echo 'GIF Created!'
	


