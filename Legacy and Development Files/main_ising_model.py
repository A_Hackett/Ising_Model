#!/usr/bin/env python

'''
----------------------------------------------------------------------------
This is the main Ising Model script
Alexander Hackett
15323791
02/11/17
----------------------------------------------------------------------------
'''



#Importing Needed Packages
#import scipy as sp
import numpy as np
#import math
import matplotlib as plt
from random import randrange, random, choice
#kb = 1.38064852e-23 #Boltzmann's constant
import time
import os.path

    
print('Running Model')

#Defining a class of all ising lattices
#the square lattices have a size, a constant temperature and a magnetic field
class ising_lattices:
    def __init__(self, size, temp, field, init_spins):
        self.size = size #side lenght of the array in atoms
        self.temp = temp #In kelvin
        self.field = field #Magnetic field in tesla
        self.init_spins = init_spins
        if self.init_spins == 'random':
            self.lattice_values = self.mk_rand()
        elif self.init_spins == 'ups':
            self.lattice_values = self.mk_up()
    def mk_rand(self): #Make an array of random spins
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = choice([1, -1]) #Randomly assign spins
        return lat
    def mk_up(self): #Make an array of up spins
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = 1 #set all spins to 1
        return lat
    def E_xy(self, x, y): #Compute the energy of some lattice point, using ising model hamiltonian
        if self.field==0: #If there's no magnetic field present energy depends on nearest only
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]))
        else: #If there is a magnetic field, energy will also depend on field strenght
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]) +
                    self.lattice_values[x, y]*self.field)

    def rand_metro(self, nsteps): #A metropolis alogorithm, randomly selecting atoms
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
    def rand_metro_ann(self, nsteps, Ts): #A metropolis alogorithm, randomly selecting atoms
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(Ts)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
    def raster_metro(self, nsteps): #A metropolis alogorithm, scanning over each atom in order
        z = 0
        while z < nsteps:
            for i in range(self.size): #Do this for as many iterations as requested
                x = i #Loop over atoms in order
                for j in range(self.size):
                    y = j
#Work out the change in energy for flipping the atom's spin, new energy minus old energy
                    E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
                    #Checking if the energy is tending to a minimum
                    if E_delta <= 0.: #Is the energy change negative?
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Does a boltzmann velocity distro allow the flip anyway?
                    elif random() < np.exp(-1. * E_delta/(self.temp)):
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Otherwise, keep the spin the same
            z += 1
    def det_mag(self): #Determines the magnitization of the lattice in its current config
        mag = 0 #Start counting the magnitization
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                mag = mag + self.lattice_values[x, y] #Add +1 for an up spin
                #And -1 for a down spin, determining magnitization
        return abs(mag) / (self.size)**2 #Return the magnitization per spin
    
    
    
temps = np.linspace(2.0,3.0, num = 20)
Ms = []
ave = 4.

for i in temps:
    ms = []
    for j in range(int(ave)):
        if j % 2 ==0:
            lattice1 = ising_lattices(50, i, 0, 'random')
        else:
            lattice1 = ising_lattices(50, i, 0, 'ups')
        lattice1.rand_metro(1000000)
        m  = lattice1.det_mag()
        ms.append(m)
    M = sum(ms)/ave
    Ms.append(M)
        #img1 = plt.imshow(lattice1.lattice_values)
        #plt.show()
    print('The Magnitization per Spin of The Lattice is ',M,' at this temperature',i)
    
name = 'Plot of Magnitization against Temperature for a ',lattice1.size,r'$\times$',lattice1.size,'Lattice'
magplot = plt.figure()
magnet = plt.plot(temps, Ms)
plt.title(name)
plt.xlabel(r'Temperature ($\frac{J}{K_{B}}$)')
plt.ylabel('Magnetization Per Spin')
plt.show()
print('Simulation Complete')
end = time.time()

elapsed = end - start
print('Elapsed Time ', elapsed, ' Seconds')




