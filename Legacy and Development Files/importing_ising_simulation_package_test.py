# -*- coding: utf-8 -*-
"""
Created on Sun Dec 10 18:49:03 2017

@author: Alex
"""

import Ising_Simulation_Package as IS
import numpy as np
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import os.path

temp = np.linspace(1.5,3.5,20)
T, E, M, HOT, SUS, = IS.Ising_Model_Simulation(temp, 5e5, 1500, 12)
print('Simulation Complete')

filename = 'MEGANEW_testing_data_v_long_run_141217.txt'
print('Writing ASCII File')
lattice_data = Table([T,E,M,HOT,SUS], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
ascii.write(lattice_data, filename)
#self emailing details
my_email = 'alexjanhackett@gmail.com'
my_passw = 'Portumna1'
recipients = ['alexjanhackett@gmail.com']
subject = 'Modular_Ising_Data_starting14/12/17'
message = 'New Data from simulation starting 14:30, 14/12/17, 12x12 array, 0.5 mil iterations, 1500 meaning iterations, 20 temp points, hopefully this finished the end of the week'
file_name = filename
#Emailing the disk file to myself!
emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)
print('Email Sent!')


