#!/usr/bin/env python

'''
----------------------------------------------------------------------------
This is the main Ising Model script
Alexander Hackett
15323791
02/11/17
----------------------------------------------------------------------------
'''
import os
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import matplotlib.pyplot as plt
#Importing Needed Packages
#import scipy as sp
import numpy as np
#import math
from random import randrange, random, choice
#kb = 1.38064852e-23 #Boltzmann's constant
import time
import os.path  

#Defining a class of all ising lattices
#the square lattices have a size, a constant temperature and a magnetic field
class ising_lattices:
    def __init__(self, size, temp, field, init_spins):
        self.size = size #side lenght of the array in atoms
        self.temp = temp #In kelvin
        self.field = field #Magnetic field in tesla
        self.init_spins = init_spins
        if self.init_spins == 'random':
            self.lattice_values = self.mk_rand()
        elif self.init_spins == 'ups':
            self.lattice_values = self.mk_up()
        self.curr_mag = 0. #The current magnitization of the lattice
        self.curr_energy = 0. #The current energy of the lattice
        self.det_mag() #Populate these with initial values
        self.det_energy() #Ditto
        self.curr_mag_sq = 0#self.curr_mag**2 #Current square of magnitization
        self.curr_energy_sq = 0#self.curr_energy**2#and for energy
    def det_energy(self):
        energy = 0 #Start counting the energy
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                energy = energy + self.E_xy(x, y) #Determine the energy at each spin
        self.curr_energy = energy
        
    def det_mag(self): #Determines the magnitization of the lattice in its current config
        mag = 0 #Start counting the magnitization
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                mag = mag + self.lattice_values[x, y] #Add +1 for an up spin
                #And -1 for a down spin, determining magnitization
        self.curr_mag = mag
        
    def set_temp(self, new_temp):
        self.temp = new_temp
        
    def mk_rand(self): #Make an array of random spins
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = choice([1, -1]) #Randomly assign spins
        return lat
    def mk_up(self): #Make an array of up spins
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = 1 #set all spins to 1
        return lat
    def E_xy(self, x, y): #Compute the energy of some lattice point, using ising model hamiltonian
        if self.field==0: #If there's no magnetic field present energy depends on nearest only
            #Using periodic boundry conditions, will swap to supercell soon
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]))
        else: #If there is a magnetic field, energy will also depend on field strenght
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]) +
                    self.lattice_values[x, y]*self.field)

    def rand_metro_relax(self, nsteps): 
        '''
        #A metropolis alogorithm, randomly selecting atoms to be flipped 
        according to selection rules, if done for enough iterations, this should
        thermalize the lattice properly, hence the function name
        '''
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
        self.det_mag() #Update the magnitization of this relaxed configuration
        self.det_energy() #Update to the energy of this relaxed configuration
        self.curr_mag_sq = self.curr_mag**2
        self.curr_energy_sq = self.curr_energy**2
    def rand_metro_measure(self, nsteps):
        '''
        This is a randomly selecting metropolis algorithm designed to measure
        4 vital characteristics of a thermalized lattice, (all thermal averages):
        The magnitization per spin, the energy per spin, the magnetic susceptibility 
        per spin and the heat capacity per spin
        Make sure to thermalize your lattice with rand_metro_relax() before
        Using this!
        Calling order:
        energy_sq, magnet_sq = rand_metro_measure(self, nsteps)
        '''
        #energy_sq = (self.curr_energy)**2 #We'll be adding energy squared values to this var
        #magnet_sq = (self.curr_mag)**2 #And we'll add the magnetizations squared to this one
        #For now, they contain the values at initial thermalization!
        #These would have been updated after thermalization, or on
        #Initialization if you havn't bothered to thermalize!!
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #check its spin
            #old_spin = self.lattice_values[x, y]
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Subtract or add 2 to the current magnitization, since we changed a spin
                self.curr_mag = self.curr_mag +  self.lattice_values[x, y]
                #subtract the energy change from the current energy, since we
                #found a lower energy state!
                self.curr_energy + self.curr_energy + 2 * E_delta
                #Add the square of the energy and magnetization changes to the
                #Squared lists
                self.curr_energy_sq = self.curr_energy_sq + np.sign(E_delta)*(E_delta**2)
                self.curr_mag_sq = self.curr_mag_sq + (self.lattice_values[x, y] * ((2 * self.lattice_values[x, y])**2))
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #have to do the same as before, since we flipped the spin anyway
                self.curr_mag = self.curr_mag +  self.lattice_values[x, y]
                self.curr_energy + self.curr_energy + 2 * E_delta
                self.curr_energy_sq = self.curr_energy_sq + np.sign(E_delta)*E_delta**2
                self.curr_mag_sq = self.curr_mag_sq + (self.lattice_values[x, y] * ((2 * self.lattice_values[x, y])**2))
                #Otherwise, keep the spin the same
                #Since we kept the spin the same, energy and magnetization don't change
                #The squared lists are local vars, so need to be returned
                #I should make them properties of this class later...
                #Update, I made them properties
         
    '''
    #THESE DON'T WORK AS OF NOW
    def rand_metro_ann(self, nsteps, Ts): #A metropolis alogorithm, randomly selecting atoms
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(Ts)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
    def raster_metro(self, nsteps): #A metropolis alogorithm, scanning over each atom in order
        z = 0
        while z < nsteps:
            for i in range(self.size): #Do this for as many iterations as requested
                x = i #Loop over atoms in order
                for j in range(self.size):
                    y = j
#Work out the change in energy for flipping the atom's spin, new energy minus old energy
                    E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
                    #Checking if the energy is tending to a minimum
                    if E_delta <= 0.: #Is the energy change negative?
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Does a boltzmann velocity distro allow the flip anyway?
                    elif random() < np.exp(-1. * E_delta/(self.temp)):
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Otherwise, keep the spin the same
            z += 1
    '''
    
print('Running Model')
start = time.time()
temps = np.linspace(1.5,3.5, num = 10)
iterations = 120000
measure_iterations = 60000
ave_magnetizations = []
ave_energies = []
magnetic_susceptibilities = []
heat_capacities = []
for i in temps: #for each temperature
    lattice1 = ising_lattices(20, i, 0, 'ups') #Make a lattice anew
    lattice1.rand_metro_relax(iterations) #Thermalize it
    lattice1.rand_metro_measure(measure_iterations)
    #Average energy per spin over all iterations, *2 to avoid double counting
    energies = (lattice1.curr_energy)/(2 * (lattice1.size**2)) 
    #Same for magnetization, with no worry about double counting this time
    magnets = (lattice1.curr_mag)/(lattice1.size**2)
    #Store the sums of all ingredients of the 4 important quantities here
    average_energy = energies
    average_magnetization = abs(magnets)
    #Needed to remove negative values tho, since -1 spin domains are the same as +1s
    energy_squares = lattice1.curr_energy_sq/(2 * (lattice1.size**2))
    magnet_squares = lattice1.curr_mag_sq/((lattice1.size**2))
    average_energy_squares = energy_squares
    average_magnetization_squares = magnet_squares
    #Now, work them out, and append to the right lists
    '''
    Magnetization = <M>/#spins
    energy = <E>/spins
    heat capacities = 1/T^2 <E^2> - <E>^2
    susceptibilities = 1/T <M^2> - <M>^2
    '''
    ave_energies.append(average_energy)
    ave_magnetizations.append(average_magnetization)
    heat_capacities.append(((1/i**2))*(average_energy_squares - average_energy**2))
    magnetic_susceptibilities.append((1/i)*((average_magnetization_squares - average_magnetization**2)))
    print('Simulation For Temperature ',i,' Complete')
print('Simulation Complete')
end = time.time()
elapsed = end - start
print('Elapsed Time ', elapsed, ' Seconds')


'''
#Writing the output to disk
filename = 'hand_pick_hot_cold_test.txt'
print('Writing ASCII File')
lattice_data = Table([temps,ave_energies,ave_magnetizations,heat_capacities,magnetic_susceptibilities], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
ascii.write(lattice_data, filename)
#self emailing details
my_email = 'alexjanhackett@gmail.com'
my_passw = 'Noy8vimz'
recipients = ['alexjanhackett@gmail.com']
subject = 'Second Real Test, go turn the PC off once this is done!'
message = 'This is the output of the second proper full test of the Ising Model. This is a 18x18 Lattice, with 88000 thermalizing iterations, and 44000 measurment iterations. These lattices will start hot or cold randomly'
file_name = filename
#Emailing the disk file to myself!
#emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)
'''


