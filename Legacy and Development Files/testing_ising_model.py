#!/usr/bin/env python

'''
----------------------------------------------------------------------------
This is the main Ising Model script
Alexander Hackett
15323791
02/11/17
----------------------------------------------------------------------------
'''
import os
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import matplotlib.pyplot as plt
#Importing Needed Packages
#import scipy as sp
import numpy as np
#import math
from random import randrange, random, choice
#kb = 1.38064852e-23 #Boltzmann's constant
import time
import os.path  

#Defining a class of all ising lattices
#the square lattices have a size, a constant temperature and a magnetic field
class ising_lattices:
    def __init__(self, size, temp, field, init_spins):
        self.size = size #side lenght of the array in atoms
        self.temp = temp #In kelvin
        self.field = field #Magnetic field in tesla
        self.init_spins = init_spins
        if self.init_spins == 'random':
            self.lattice_values = self.mk_rand()
        elif self.init_spins == 'ups':
            self.lattice_values = self.mk_up()
        self.curr_mag = np.float64(0)
        self.curr_energy = np.float64(0)
        self.det_energy()
        self.det_mag()
        self.t_ave_e = np.float64(0)
        self.t_ave_m = np.float64(0)
        self.mlist = []
        self.elist = []
        self.mag_sus = np.float64(0)
        self.heat_cap = np.float64(0)
    def det_energy(self):
        energy = np.float64(0) #Start counting the energy
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                energy = energy + self.E_xy(x, y) #Determine the energy at each spin
        self.curr_energy = energy
        
    def det_mag(self): #Determines the magnitization of the lattice in its current config
        mag = np.float64(0) #Start counting the magnitization
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                mag = mag + self.lattice_values[x, y] #Add +1 for an up spin
                #And -1 for a down spin, determining magnitization
        self.curr_mag = mag 
        
    def set_temp(self, new_temp):
        self.temp = new_temp
        
    def mk_rand(self): #Make an array of random spins
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = choice([1, -1]) #Randomly assign spins
        return lat
    def mk_up(self): #Make an array of up spins
        lat = np.zeros((self.size, self.size), dtype=int) # sizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                lat[x, y] = 1 #set all spins to 1
        return lat
    def E_xy(self, x, y): #Compute the energy of some lattice point, using ising model hamiltonian
        if self.field==0: #If there's no magnetic field present energy depends on nearest only
            #Using periodic boundry conditions, will swap to supercell soon
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]))
        else: #If there is a magnetic field, energy will also depend on field strenght
            return (-1.0 * self.lattice_values[x, y]*
                    (self.lattice_values[(x + 1) % self.size, y] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y] +
                     self.lattice_values[x, (y + 1) % self.size] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size]) +
                    self.lattice_values[x, y]*self.field)

    def rand_metro_relax(self, nsteps): 
        '''
        #A metropolis alogorithm, randomly selecting atoms to be flipped 
        according to selection rules, if done for enough iterations, this should
        thermalize the lattice properly, hence the function name
        '''
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
        self.det_mag() #Update the magnitization of this relaxed configuration
        self.det_energy() #Update to the energy of this relaxed configuration
    def rand_metro_measure(self, nsteps):
        '''
        This is a randomly selecting metropolis algorithm designed to measure
        4 vital characteristics of a thermalized lattice, (all thermal averages):
        The magnitization per spin, the energy per spin, the magnetic susceptibility 
        per spin and the heat capacity per spin
        Make sure to thermalize your lattice with rand_metro_relax() before
        Using this!
        Calling order:
        energy_sq, magnet_sq = rand_metro_measure(self, nsteps)
        '''
        #energy_sq = (self.curr_energy)**2 #We'll be adding energy squared values to this var
        #magnet_sq = (self.curr_mag)**2 #And we'll add the magnetizations squared to this one
        #For now, they contain the values at initial thermalization!
        #These would have been updated after thermalization, or on
        #Initialization if you havn't bothered to thermalize!!
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            flip = False #We haven't flipped this one yet
            #check its spin
            #old_spin = self.lattice_values[x, y]
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                flip = True #We flipped it
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                flip = True #We flipped it
            if flip: #If we flipped, need to update energies and mags
                self.curr_energy += E_delta
                self.curr_mag += self.lattice_values[x, y]
                self.mlist.append(self.curr_mag)
                self.elist.append(self.curr_energy)
        self.t_ave_e = self.curr_energy / self.size**2
        self.t_ave_m = self.curr_mag / self.size**2
        ave_e = np.average(self.elist)
        ave_m = np.average(self.mlist)
        ave_e_sq = ave_e**2
        ave_m_sq = ave_m**2
        ave_m_to_be_squared = sum(np.array(self.mlist)**2)/len(self.mlist)
        ave_e_to_be_squared = sum(np.array(self.elist)**2)/len(self.elist)
        self.mag_sus = (self.size**(-2)*(1.0/self.temp))*(ave_m_to_be_squared-ave_m_sq)
        self.heat_cap = (self.size**(-2)*(1.0/self.temp)**2)*(ave_e_to_be_squared-ave_e_sq)
        
         
    '''
    #THESE DON'T WORK AS OF NOW
    def rand_metro_ann(self, nsteps, Ts): #A metropolis alogorithm, randomly selecting atoms
        for i in range(nsteps): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(Ts)):
                self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                #Otherwise, keep the spin the same
    def raster_metro(self, nsteps): #A metropolis alogorithm, scanning over each atom in order
        z = 0
        while z < nsteps:
            for i in range(self.size): #Do this for as many iterations as requested
                x = i #Loop over atoms in order
                for j in range(self.size):
                    y = j
#Work out the change in energy for flipping the atom's spin, new energy minus old energy
                    E_delta = - 2 * self.E_xy(x, y) #Flip the spin (*-1) and subract original energy
                    #Checking if the energy is tending to a minimum
                    if E_delta <= 0.: #Is the energy change negative?
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Does a boltzmann velocity distro allow the flip anyway?
                    elif random() < np.exp(-1. * E_delta/(self.temp)):
                        self.lattice_values[x, y] = self.lattice_values[x, y] * -1 #Flip the spin
                        #Otherwise, keep the spin the same
            z += 1
    '''
    
print('Running Model')
start = time.time()
mean_energy = []
mean_magnet = []
mean_heat_cap = []
mean_magnetic_sus = []
for j in range(5):
    temps = np.linspace(1.5,3.5, num = 5)
    iterations = round(5e4)
    measure_iterations = round(3.5e4)
    ave_magnetizations = []
    ave_energies = []
    magnetic_susceptibilities = []
    heat_capacities = []
    for i in temps: #for each temperature
        lattice1 = ising_lattices(10, i, 0, 'random') #Make a lattice anew
        lattice1.rand_metro_relax(iterations) #Thermalize it
        lattice1.rand_metro_measure(measure_iterations) #measure it
       
        '''
        Magnetization = <M>/#spins
        energy = <E>/spins
        heat capacities = 1/T^2 <E^2> - <E>^2
        susceptibilities = 1/T <M^2> - <M>^2
        '''
        ave_energies.append((lattice1.t_ave_e))
        ave_magnetizations.append(abs(lattice1.t_ave_m))
        heat_capacities.append(lattice1.heat_cap)
        magnetic_susceptibilities.append(lattice1.mag_sus)
        print('Simulation For Temperature ',i,' Complete')
    mean_energy.append(ave_energies)
    mean_magnet.append(ave_magnetizations)
    mean_heat_cap.append(heat_capacities)
    mean_magnetic_sus.append(magnetic_susceptibilities)
print('Simulation Complete')
mean_energy = np.array(np.mean(mean_energy, axis = 0))
mean_magnet = np.array(np.mean(mean_magnet, axis = 0))
mean_heat_cap = np.array(np.mean(mean_heat_cap, axis = 0))
mean_magnetic_sus = np.array(np.mean(mean_magnetic_sus, axis = 0))
end = time.time()
elapsed = end - start
print('Elapsed Time ', elapsed, ' Seconds')



#Writing the output to disk
filename = '50x50_second_working_test.txt'
print('Writing ASCII File')
lattice_data = Table([temps,mean_energy,mean_magnet,mean_heat_cap,mean_magnetic_sus], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
ascii.write(lattice_data, filename)
#self emailing details
my_email = 'alexjanhackett@gmail.com'
my_passw = 'Noy8vimz'
recipients = ['alexjanhackett@gmail.com']
subject = 'First Totally Working test 50x50'
message = 'This is the output of a 50x50 lattice, thermalized one million iterations, measured half a million iterations, 100 data points between 1.5 and 3.5 J/kb. This is the first test run for which the mag sus and heat capacity work correctly!!'
file_name = filename
#Emailing the disk file to myself!
#emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)



