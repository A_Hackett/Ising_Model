#!/bin/bash
#Author: Alexander Hackett 
#Student Number  15323791 
#ahackett@tcd.ie 
#Created for Prof. Archers assignment, Ising Model Simulation

#Author: Alexander Hackett,
#15323791
#ahackett@tcd.ie
#Created for Prof. Archer's Assignment, Ising Model Simulation
#
#This Shell script will run the 3D Ising Lattice visualization, to visualise the 3D Ising Lattice

echo 'This is the 3D Ising Model Visualizer'
read -p 'How Large a 3D Lattice to Visualize? ' size
read -p 'What Temperature to Thermalize the Ising Lattice at? ' temp
read -p 'What External Magnetic Field to Subject the Lattice to? ' field
read -p 'Type "random" to Start the Lattice of in a Random Configuration, of "ups" to Start in an all Spins up Configuration ' start
read -p 'Finally, How Many Iterations to Thermalize the Lattice for? ' iterations

echo 'Running Simulation'
python Visualize_3D_Ising_Lattice.py $temp $size $field $start $iterations

