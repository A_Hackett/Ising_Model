#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''
import numpy as np
import matplotlib as mpl
mpl.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import stats
import os
import glob

'''
This is a script that takes the output files from the numerous runs of the finite size
measuring script, and combines them to produce plots of the finite size effect on the
magnetic susceptability peak values and the heat capacity peak values, and then determines the
Critical temperature via linear regression
'''

#wrapping everything in main
def main():
    #First take the heat capacities
    peak_temps = []
    sizes = []
    #Glob the filenames according to the convension in the python script
    for filename in glob.glob('heatcapacity*lattice_for_measurement_of_lattice_size.txt'):
        size = int(filename[12:14]) #Slice out the size of the lattice
        sizes.append(size) #append to the array of lattice sizes
        data = np.genfromtxt(filename,delimiter=' ') #Read the ASCII files
        temps = data[1:,0] #Skip the header
        heat_caps = data[1:,1]
        peak_temp = temps[np.where(heat_caps == max(heat_caps))] #Find the heat capacity peaks
        peak_temps.append(peak_temp[0]) #And keep those peak temperatures
        
        
    reg_to_inf_plot = plt.figure()
    in_sizes = [1./i for i in sizes] #Take the inverse of the lattice size
    plt.plot(in_sizes, peak_temps, 'o') #Plot the peak temperature vs inverse lattice size
    fitm, fitc, _,__,___ = stats.linregress(in_sizes, peak_temps) #Perform linear regression
    fitys = (np.array(in_sizes) * fitm) + fitc
    lab = 'Best Linear Fit, Y Intercept: ' + str(fitc) #Display the y intercept, the value at infinate lattice size
    plt.plot(in_sizes, fitys, '--', label = lab)
    plt.xlabel(r'$\frac{1}{L}$, Inverse Size of Lattice')
    plt.ylabel(r'$T_{c}$, Critical/Peak Temperature in Normalized Units')
    plt.title(r'Determination of $T_{c}$ using Size Effect of Lattice on Heat Capacity Peaks')
    plt.legend()
    
    #Analagously for the Magentic Suspectabilities
    peak_temps = []
    sizes = []
    for filename in glob.glob('suscept*lattice_for_measurement_of_lattice_size.txt'):
        size = int(filename[7:9])
        sizes.append(size)
        data = np.genfromtxt(filename,delimiter=' ')
        temps = data[1:,0] #Skip the header
        suses = data[1:,1]
        peak_temp = temps[np.where(suses == max(suses))]
        peak_temps.append(peak_temp[0])
        
        
    reg_to_inf_plot_sus = plt.figure()
    in_sizes = [1./i for i in sizes]
    plt.plot(in_sizes, peak_temps, 'o')
    fitm, fitcs, _,__,___ = stats.linregress(in_sizes, peak_temps)
    fitys = (np.array(in_sizes) * fitm) + fitcs
    lab = 'Best Linear Fit, Y Intercept: ' + str(fitcs)
    plt.plot(in_sizes, fitys, '--', label = lab)
    plt.xlabel(r'$\frac{1}{L}$, Inverse Size of Lattice')
    plt.ylabel(r'$T_{c}$, Critical/Peak Temperature in Normalized Units')
    plt.title(r'Determination of $T_{c}$ using Size Effect of Lattice on Magnetic Susceptability Peaks')
    plt.legend()
    
    print((fitcs + fitc)/2.,'Is the Average Y Intercept, and hence the expected Critical Temperature')
    
if __name__ == '__main__':
    main()