#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''
'''
This package facilitates visualising the initial and final solutions of a
TSP
'''
import matplotlib as mpl
mpl.use('Qt5Agg')
import matplotlib.pyplot as plt #Only real dependancy
import numpy as np #We might be dealing with numpy arrays passed to us by the TSP package
class vis_anneal:
    '''
    A class of final states of a TSP, for visualization
    '''
    def __init__(self, input_coords, initial_route, initial_length, final_route, final_length, random_start=False):
        '''
        Very Lengthy initilization parses all important information from the TSP 
        solution in order to allow plotting
        '''
        self.input_coords = input_coords #The city coordinates in the order supplied
        #before the TSP was solved
        self.initial_route = initial_route #The first route, either greedy or random
        self.initial_length = initial_length#Its length
        self.final_route = final_route#The final, annealed route
        self.final_length = final_length# Its length
        self.is_rand = random_start#Did we have a random start?
        
        #Order the cities by the initial route
        self.initial_order_cities = [self.input_coords[i] for i in self.initial_route]
        #And by the annealed route
        self.final_order_cities = [self.input_coords[i] for i in self.final_route]
        #Extract the x coordinates from ordered initial cities
        self.initial_x_coords = list(zip(*self.initial_order_cities))[0]
        #now, append the first city back to the end of the route, to close the loop (Nicer Visually)
        #np.append(self.initial_x_coords, self.initial_x_coords[0])
        #Do the same with the y coords
        self.initial_y_coords = list(zip(*self.initial_order_cities))[1]
        #np.append(self.initial_y_coords, self.initial_y_coords[0])
        #And the same again for the annealed solution
        self.final_x_coords = list(zip(*self.final_order_cities))[0]
        #np.append(self.final_x_coords, self.final_x_coords[0])
        self.final_y_coords = list(zip(*self.final_order_cities))[1]
        #np.append(self.final_y_coords, self.final_y_coords[0])
        self.units = []
        #What units are these coordinates in? Default to just generic 'Units'
        self.set_units('Units')
        self.x_name = ''
        self.y_name = ''
        #What are the x and y coordinates called? Eg Longtitude and Latitude?, default to just X and Y
        self.set_xy_name('X','Y')
        
        
    def set_xy_name(self, xname, yname):
        '''
        This method sets the name of the X and Y coordinates, to be used on the axis 
        of the plot
        '''
        #Ensure we've been passed  strings
        if type(xname) == str and type(yname) == str:
            self.x_name = xname
            self.y_name = yname
        else:#Otherwise, problems
            raise TypeError('Dataset Names Must be Strings!')
        
    def set_units(self, units):
        '''
        And, what units are all these coordinates in?
        '''
        #Once again, looking for a string here
        if type(units) == str:
                self.units = units
        else:
            raise TypeError('The Unit Name Passed Must be a String!')
            
            
    def vis_data(self):
        '''
        With everything else sorted, this method produces the two graphs,
        one of the initial route, and one of the final, annealed route!
        '''
        #Construct the label
        label_in = 'Length = ' + str(self.initial_length) + self.units
        Initial_data_plot = plt.figure()
        #Plot the data as points, joined by lines
        plt.plot(self.initial_x_coords,self.initial_y_coords, '-o', label = label_in)
        if not self.is_rand: #Set the appropriate title
            plt.title('Plot of Initial Greedy Route')
        else:
            plt.title('Plot of Initial Random Route')
        #And the appropriate axis labels
        plt.xlabel(self.x_name + ' ('+self.units + ')')
        plt.ylabel(self.y_name + ' ('+self.units + ')')
        plt.legend() #And make the legend
        
        Final_data_plot = plt.figure()
        #Analagous to before
        label_fin ='Length = ' + str(self.final_length) + self.units
        plt.plot(self.final_x_coords, self.final_y_coords, '-o', label = label_fin)
        plt.title('Plot of Final Annealed Route')
        plt.xlabel(self.x_name + ' ('+self.units + ')')
        plt.ylabel(self.y_name + ' ('+self.units + ')')
        plt.legend()
        #Plot them!
        plt.show()

        
        
        
