#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

import os
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import numpy as np
from random import randrange, random, choice
import time
import os.path  

#Make a class for simulating 1D ising models
class ONED_Ising_Lattice:
    '''
    This is a class for simulating 1 dimensional Ising Lattices/chains
    The chains are created at a fixed length and temperature, with an intial
    spin configuration and a background magnetic field, if desired
    All units are normalized, (J = Kb = T = 1)
    '''
    def __init__(self, length, temp, field, init_spins):
        self.length = length #Length of the chain of atoms
        self.temp = temp #Temeprature in normalized units
        self.field = field #magnetic field in normalized units
        self.init_spins = init_spins
        if self.init_spins == 'random':
            self.lattice_values = self.mk_rand() #randomize the spins on the chain
        elif self.init_spins == 'ups':
            self.lattice_values = self.mk_up() #Make all the spins on the chain +1
        self.curr_mag = np.float64(0) #Tracking the excess of spins
        self.curr_energy = np.float64(0) #And the energy
        self.det_energy() #Determine the energy of the initial configureations, expect ~-1 for random, ~-4 for all up
        self.det_mag() #initialize the magnitization, expect 1 for all ups ~0 for random
        self.t_ave_e = np.float64(0) #This will hold the averaged energy
        self.t_ave_m = np.float64(0) #And the anveraged mangitization
        self.mlist = [] #This is a list of all the magnitization measurements
        self.elist = [] #And the same for the energy measurements
        self.mag_sus = np.float64(0) #The determined value fro the magnetic susceptiability will be put in here
        self.heat_cap = np.float64(0) #And the same for the heat capacity
        
    def det_energy(self):
        '''
        This is a method that sweeps the whole chain in place
        and determines the total energy of the current chain configuration
        '''
        energy = np.float64(0) #Start counting the energy
        for x in range(self.length): #For every spin in the chain
            energy += self.E_x(x) #Tack on the energy of that spin
        self.curr_energy = energy / 2. #/2 to prevent double counting And store that total energy in this property
        
    def det_mag(self): #Analagous for the magnitization
        '''
        This is a method that sweeps the entire chain and determines the overall
        magnitization of the chain, which can be posative or negative
        '''
        mag = np.float64(0)
        for x in range(self.length):
            mag += self.lattice_values[x] #Simple add on the spin, since -1 will cancel a 1 and vice versa
        self.curr_mag = mag
        
    def set_temp(self, new_temp):
        '''
        This is a simple method that changes the temperature of the chain
        to the given normalized temperature
        '''
        self.temp = new_temp
        
    def mk_rand(self): #Make an array of random spins
        '''
        This is a method which initializes the chain with a random arrangment
        of spins
        '''
        lat = np.zeros((self.length), dtype=int) # length chain of zeros
        for x in range(self.length): #Loop over x values
            lat[x] = choice([1, -1]) #Randomly assign spins
        return lat
    def mk_up(self): #Make an array of up spins
        '''
        This method initializes the chain so that all the spins are up
        '''
        lat = np.zeros((self.length), dtype=int) # length chain of zeros
        for x in range(self.length): #Loop over x values
            lat[x] = 1 #set all spins to 1
        return lat
    
    def E_x(self, x):
        '''
        This method determines the energy of a single spin, at position x on the
        chain by comparing its spin to the spin of its two neighbours
        The magnetic internal energy of the spin (spin * magnetic field) is also
        included when there is an external field present
        '''
        if self.field ==0:
            return (-1.0 * self.lattice_values[x] *
                    (self.lattice_values[(x+1) % self.length] + 
                    self.lattice_values[(x - 1 + self.length) % self.length]))
        else:
            return (-1.0 * self.lattice_values[x] *
                     (self.lattice_values[(x+1) % self.length] + 
                     self.lattice_values[(x - 1 + self.length) % self.length]) + 
                     self.lattice_values[x]*self.field)
            
    def rand_metro_relax(self, nsteps):
        '''
        This method thermalizes the chain over a number of iterations
        nsteps, and does not take any measurements while doing so
        Once thermalization is complete, the energy and magnitization of the chain
        are updated
        '''
        for i in range(int(nsteps)): #Only allowed to iterate over an integer
            x = randrange(self.length) #Pick a random spin
            E_delta = -2.0 * self.E_x(x) #Determine the energy change if the spin were flipped
            
            if E_delta <= 0.: #If the energy is negative/zero
                self.lattice_values[x] = self.lattice_values[x] * -1 #Flip the spin
            elif random() < np.exp(-1. * E_delta/(self.temp)): #if a boltzmann velocity/energy disto allows the flip anyway
                self.lattice_values[x] = self.lattice_values[x] * -1 #flip the spin
                
        self.det_mag() #Update the magnitization
        self.det_energy() #update the energy
            
    def rand_metro_measure(self, nsteps):
        '''
        This method determines the average energy, magnetization,
        heat capacity and magnetic susceptability per spin on the chain by performing
        the metropolis algorithm nsteps times.
        '''
        for i in range(int(nsteps)): #can only iterate over an integer
            x = randrange(self.length) #Pick a random spin
            flip = False #its not been flipped (yet)
            E_delta = -2.0 * self.E_x(x) #Determine the energy change from flipped the spin
            
            if E_delta <= 0.: #If negative energy change
                self.lattice_values[x] = self.lattice_values[x] * -1 #Flip the spin
                flip = True #We flipped the spin
            elif random() < np.exp(-1. * E_delta/(self.temp)): #If boltzmann allows it anyway
                self.lattice_values[x] = self.lattice_values[x] * -1 #Flip the spin
                flip = True #We flipped it
                
            if flip: #If we flipped the spin
                self.curr_energy += E_delta / 2 #/2 to prevent double counting in the long run Add on the energy
                self.curr_mag += self.lattice_values[x] #And the magnitization
                self.mlist.append(self.curr_mag) #Add the magnitization value to our running list
                self.elist.append(self.curr_energy) #Add the energy value to our running list
                #Using running lists is going to be quicker than rechecking the full chain each time
                #(At least it was for the 2D lattice...)
        #Once all the measuring iterations are finished
        self.t_ave_e = self.curr_energy / self.length#Work out the energy per spin
        self.t_ave_m = self.curr_mag / self.length#And the magnitization per spin
        ave_e = np.average(self.elist)#To find the average energy overall, 
        #just average the running list of energies (Since they all happened at the same temp)
        ave_m = np.average(self.mlist)
        #Likewise for finding the average magnitization
        ave_e_sq = ave_e**2 #The average energy, squared
        ave_m_sq = ave_m**2 #The average magnitization squared
        try: #We might have no enteries in the mlist, meaning no flips
            #Will produce a divide by zero
            #This is the average of the magnitization squared <M^2>
            ave_m_to_be_squared = sum(np.array(self.mlist)**2)/len(self.mlist)
        except ZeroDivisionError: #Catch the divide by zero
            #means we didn't do any flips, so the magnitization squared average should just be zero
            ave_m_to_be_squared = np.float64(0)
        try: #Same reasoning as before
            #This is the average of the energy squared <E^2>
            ave_e_to_be_squared = sum(np.array(self.elist)**2)/len(self.elist)
        except ZeroDivisionError: #Catch the divide by zero
            #No flips, so just set this to zero
            ave_e_to_be_squared = np.float64(0)
        #Setting those to zero ensures we get zero for these two values here, if we
        #do no flips, which makes sense, if we're not flipping at all, either we
        #have a very strong field, in which case mag_sus and heatcap should be zero
        #Or we have a very low temperature, and they should be zero
        #1D versions of these formulae, since we have length spins, not size^2 like in 2D
        self.mag_sus = (self.length**(-1)*(1.0/self.temp))*(ave_m_to_be_squared-ave_m_sq)
        self.heat_cap = (self.length**(-1)*(1.0/self.temp)**2)*(ave_e_to_be_squared-ave_e_sq)
        '''
            Magnetization = <M>/#spins
            energy = <E>/spins
            heat capacities = 1/T^2 <E^2> - <E>^2
            susceptibilities = 1/T <M^2> - <M>^2
        '''
        
def ONED_ISING_SIM(temps, iterations, meaning_iterations, length):
    '''
    This is a helper function that allows the actual ising simulation to be
    easily run. It takes an array of temperatures to be simulated, from low to high,
    and a number of iterations to thermalize at each temp. It then measures for
    half that number of iterations, before moving onto the next temp. It does this
    meaning_iterations times, in order to get a good average value at each temperature,
    and remove as much noise as possible. Length is the desired number of atoms in
    the chain
    '''
    mean_energy = [] #The average energies from all meaning_iteration
    mean_magnet = [] #The average magnitizations for the same
    mean_heat_cap = [] #Same, for heat cap
    mean_magnetic_sus = [] #Same, for magsus
    for j in range(meaning_iterations): #Do this for as many meaning iterations as desired
        temps = temps #TODO, remove
        iterations = round(iterations) #Has to be an integer, iterable
        measure_iterations = round(iterations/2) #Same
        ave_magnetizations = [] #magnitization for this meaning iteration
        ave_energies = [] #energy for this meaning iteration
        magnetic_susceptibilities = [] #Same for magsus
        heat_capacities = [] #and heatcap
        #Create a 1D ising chain instance, start at the first temperature
        #No field, all spins up
        lattice1 = ONED_Ising_Lattice(length, temps[0], 0, 'ups')
        for i in temps: #for each temperature
            lattice1.set_temp(i) #Set the chain to the new temp
            lattice1.rand_metro_relax(iterations) #Thermalize it
            lattice1.rand_metro_measure(measure_iterations) #measure it
           
            
            ave_energies.append((lattice1.t_ave_e)) #Add the average energy at this temp
            #To the list of energies
            ave_magnetizations.append(abs(lattice1.t_ave_m))
            #Do the same for magnitizations
            heat_capacities.append(lattice1.heat_cap)
            #Same again
            magnetic_susceptibilities.append(lattice1.mag_sus)
            #print('Simulation For Temperature ',i,' Complete') #Debugging
        mean_energy.append(ave_energies)
        mean_magnet.append(ave_magnetizations)
        mean_heat_cap.append(heat_capacities)
        mean_magnetic_sus.append(magnetic_susceptibilities)
        #Debugging/benchmarking
        print('Simulation Complete for Iteration', j,' of ',meaning_iterations)
    #Average the corresponding values for each measurable, at each temperature
    #over all the meaning iterations, to get a nice, clean average
    mean_energy = np.array(np.mean(mean_energy, axis = 0))
    mean_magnet = np.array(np.mean(mean_magnet, axis = 0))
    mean_heat_cap = np.array(np.mean(mean_heat_cap, axis = 0))
    mean_magnetic_sus = np.array(np.mean(mean_magnetic_sus, axis = 0))
    #Return each value as an output
    return temps, mean_energy, mean_magnet, mean_heat_cap, mean_magnetic_sus


#Testing the function out
def main():
    temps = np.linspace(0,10, 100) #40 temperatures
    #Run the simulation
    temps, mean_energy, mean_magnet, mean_heat_cap, mean_magnetic_sus = ONED_ISING_SIM(temps, 1e5, 10, 100)
    #Writing the output to disk
    filename = 'Large_temp_range_1D_data.txt'
    print('Writing ASCII File')
    #make an ascii table
    lattice_data = Table([temps,mean_energy,mean_magnet,mean_heat_cap,mean_magnetic_sus], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
    #Write the table to disk
    ascii.write(lattice_data, filename)
    #self emailing details
    my_email = 'alexjanhackett@gmail.com'
    my_passw = 'Portumna1' #TODO, remove before submission
    recipients = ['alexjanhackett@gmail.com']
    subject = 'Second One-D Ising Model Test'
    message = 'This is the output of a 100 atom chain, thermalized 2e5 iterations, measured 1e5 iterations, each meaned 50 times, over a temperature range from 1.5 to 3.5 with 40 steps. I am reciving this email and file because I ran Ising_Simulation_1D_Package.py as a programme rather than importing it! Hopefully the data is still of some use!! '
    file_name = filename
    #Emailing the disk file to myself!
    emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)

if __name__ == '__main__':
    main()
        
        
                
                
        
