#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This Script invokes the 3D_Ising_Model_Visualizer.py package in order
to visualise the thermalization of a 3D Ising Lattice, This script is to be 
invokes with the shell script wrapper, 3D_Vis.sh
'''
#This is needed to take command line arguments
import sys
#And this is the visualization package
import ThreeD_Ising_Model_Visualizer as Ising3D

def main():
    #parse the command line arguments
    temperature, size, field, start, iterations = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]
    #And ensure the start is a string
    start = str(start)
    #Run the Visualizer
    Ising3D.vis(temperature, size, field, start, iterations)
    print('Images Generated!')
    
if __name__ == '__main__':
    main()