#!/bin/bash

#Author: Alexander Hackett 
#Student Number  15323791 
#ahackett@tcd.ie 
#Created for Prof. Archers assignment, Ising Model Simulation

#Author: Alexander Hackett,
#15323791
#ahackett@tcd.ie
#Created for Prof. Archer's Assignment, Ising Model Simulation

#This shell script fetches the csv file containing all the irish towns beginning with A, from my personal webserver, and saves it to the
#current directory.
#The script then uses the appropriate python script in order to anneal a solution to the Travelling Salesman problem for the first N
#Towns in that csv file, as determined by user input!

#Ask how many towns to visit
read -p 'Number of Towns to Visit?: ' numb_towns
#Error checking, we need at least 4 towns to the route swapping algorithm to work!, the python script already checks for this error, but
#better to be safe than sorry
if [ "$numb_towns" -lt 4 ]; then
	echo 'Please Pass an Integer Greater than 4 Through the CLI!'
	echo 'The Route Swapping Algorithm Doesn"t Work with less than 4 Towns'
	exit
fi

#check if the csv file, containing all the town coordinates exists
if [ ! -f ./ie-towns-sample.csv ]; then
	#If it's not there, would you like to download it from my webserver?
    read -p ".csv file not found in the current directory, would you like to download it from my webserver? [y/n] " yes_no
	
	if [ "$yes_no" == "y" ]; then #Download the file
		echo 'Downloading File'
		wget 'http://86.40.64.214/ie-towns-sample.csv'
	else #If no, end the program
		echo 'Terminating Program'
		exit
	fi
fi

#Run the TSP script, which uses the TSP solver and visualizer scripts
echo 'Running TSP Script for ' $numb_towns ' Towns'
#Run the script, passing the command line argument
python Irish_Towns_TSP.py $numb_towns #Just invoke the python interperator directly, better cross platform compatiblity

