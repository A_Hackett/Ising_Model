# Ising Model Assignment

* Alexander Hackett
* 15323791
* ahackett@tcd.ie


This is the README for the Repo containing the code written for Prof. Archer's PY3C01 Module Assignment. The scripts and packages in the repo were utilized in order to produce all of the data, and many of the visualizations used throughout the assignment and the report.


The Full Report is additionally included in the repo, [here](Ising_Model_Assignment_AlexanderHackett_15323791.pdf)

In the Assignment, the Phase Transition of the 2 Dimensional Ising Lattice was investigated in detail. The effects of changing the size of that lattice on the phase transition in 2D were also investigated. Additionally, the effect of performing an analagous Monte Carlo simulation using the Metropolis Algorithm on a 1 Dimensional Ising Chain, and 3 Dimensional Ising Lattice were investigated.

Finally, as a demonstration of the utility of an Ising Lattice based simulation, a heuristic known as Simulated Annealing was utilized to anneal a solution to the ground state of an non-trivial Ising Lattice based problem, the Travelling Salesman Problem.

## Visualization
These are the gifs generated to visualize the thermalization of the 2 Dimensional Ising Lattice as mentioned in the acompanying report!


![Cold Lattice](Ising_Animation_2D_50x50_cold.gif)


This is the Lattice thermalizing at a very low temperature, around 1




![Hot Lattice](Ising_Animation_2D_50x50_hot.gif)

This is the lattice thermalizing at a very high temperature, around 10




![Critical Lattice](Ising_Animation_2D_50x50_critical.gif)

This is the lattice thermalizing around the critical temperature, 2.269. Note the formation of domains, and the presence of long range and complex interations

## The Codebase

The majority of the files contained in the repo were written in Python 3.6.3. Although not all files have been tested, the majority should be compatiable with Python 2.7. The remaining files are shell scripts, written for bash, primarily used to invoke the Python scripts from the command line with additional interactivity etc, as well as automating tasks such as adding headers to Python files and running the same Python script many times with different parameters.

There are additionally some Python scripts that were complied to .pyc files in CPython, for a minute performance increase. These were compiled with Conda 3, and hence are not compatible with Python 2, due to having the wrong magic number.

The folder 'Legacy and Development Files' contains obselete and redundant files, or files that were utilized for testing at some point during the development of this codebase.


### Prerequisites

Possibly uncommon Python dependancies include:
* Astropy
* QT5Agg Matplotlib Backend

Additionally the [TSP_Irish_Towns.sh](TSP_Irish_Towns.sh) script for annealing a solution to the travelling salesman problem for a number of towns in Ireland requires the file ie-towns-sample.csv in your user directory, which is included in this repo. If this file isn't present in your user directory when running the script, the script will prompt you to download it from my webserver. If my server isn't online, you can email me at ahackett@tcd.ie or ping me on IRC on Freenode at AlexHackett. 



### Installing

All scripts should run fine out of the box, provided they are all in the working directory. Every Python package dependancy can be downloaded via pip.


## Instructions
The Majority of the files in this repo are packages. There are a couple of wrapper files, written in both Python and Bash for running the appropriate simulations.

* [Ising_Model_1D_Data_Gather.py](Ising_Model_1D_Data_Gather.py)
* [Ising_Model_2D_Data_Gather.py](Ising_Model_2D_Data_Gather.py)
* [Ising_Model_3D_Data_Gather.py](Ising_Model_3D_Data_Gather.py)
These are the scripts for simulating and gathering data on the 1, 2 and 3 D Ising Models respectivly. The estimated runtime of each of these scripts is very long at the moment, and they were generally run overnight in order to generate the results seen in the report



* [finite_size.sh](finite_size.sh)
This is the script used to run the simulation and gather data on the 2D Ising Model for a variety of Lattice sizes. This was used to make an accurate estimation of the Critical Temperature, using finite size scaling and linear regression. As before, the runtime of this is exceedingly large. In practice, the wrapped script, [Measure_2D_Ising_Model.py](Measure_2D_Ising_Model.py) was run 'by hand' several times, so that the data could be analysed _etc_ as it came in. [peak_getter.py](peak_getter.py) was then run manually to analyse the data files and determine the critical temperature


# The Visualization Scripts
The Following scripts were used to visualise the thermalization of the Ising Model, and in the case of the 2D Ising Lattice, Produce an animated gid

* [3D_Vis.sh](3D_Vis.sh)
This script provides a 'before and after' snapshot of the 3D Ising Lattices thermalization

* [animated_2D.sh](animated_2D.sh)
This script produces an animated gif of the thermalization of the 2D Ising Lattice

# Simulated Annealing and the TSP
The Following Script runs and visualizes the results of Annealing a Solution to the Travelling Salesman problem for a number of Irish towns (each beginning with the letter A!)

* [TSP_Irish_Towns.sh](TSP_Irish_Towns.sh)



## Author

* **Alexander Hackett**
* Student Number: 15323791
* ahackett@tcd.ie


