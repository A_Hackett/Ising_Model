#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This package allows the visualization for the thermalization of a 
3D ising Model
'''
#Importing needed packages
import matplotlib as mpl
mpl.use('Qt5Agg') #Required backend to run properly on comphy lab computers
from mpl_toolkits.mplot3d import Axes3D #3D Plotting tools
import matplotlib.pyplot as plt
#The three dimensional Plotting package
import Ising_Simulation_3D_Package as IS3D
import sys
import numpy as np


class ThreeD_Vis:
    '''
    This is the Class for Visualziation of a 3D Ising Lattice
    '''
    def __init__(self, temperature, size, field, iterations, start):
        '''
        Run the Ising Simulation, and collect the data as needed
        '''
        self.threeD_lat = IS3D.Ising_Lattice_3D(size, temperature, field, start)
        self.threeD_lat.rand_metro_relax(iterations)
        self.config = self.threeD_lat.lattice_values
        
    def visualize(self):
        '''
        Plot the configuration of the Ising Lattice after thermalization
        '''
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111, projection='3d')
        pos = np.where(self.config==1)
        ax1.scatter(pos[0], pos[1], pos[2], c='black')
        pos1 = np.where(self.config==-1)
        ax1.scatter(pos1[0], pos1[1], pos1[2], c='red')
        

def vis(temperature, size, field, start, iterations):
    '''
    Helper function to streamline visualization of the thermalizing of
    the three dimensional Ising lattice
    '''
    #Ensure the temperature passed via sys.argv is a float
    temperature = float(temperature)
    #And that the size is an int
    size = int(size)
    #And that the field is a float
    field = float(field)
    iterations = int(iterations)
    #Create the visaulizing object, for zero iterations (initial config)
    visual = ThreeD_Vis(temperature, size, field,0 ,start)
    #And visualize it
    visual.visualize()
    #Add a title
    plt.title('Initial Configuration')
    #Repeat for the thermalized lattice, with n iterations
    visual = ThreeD_Vis(temperature, size, field,iterations ,start)
    visual.visualize()
    plt.title('Final Configuration')
    plt.show()
        
def main():
    '''
    Just a testing Function Wrapped in main()
    '''
    vis(0, 3, 0, 'random', 100000)
    

if __name__ == '__main__':
    main()

        
        
        

