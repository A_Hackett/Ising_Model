#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This script allows the visualization for the thermalization of a 
3D ising Model
'''

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import Ising_Simulation_3D_Package as IS3D
import sys
import numpy as np


class ThreeD_Vis:
    '''
    This is the Class for Visualziation of a 3D Ising Lattice
    '''
    def __init__(self, temperature, size, field, iterations, start):
        '''
        Run the Ising Simulation, and collect the data as needed
        '''
        self.threeD_lat = IS3D.Ising_Lattice_3D(size, temperature, field, start)
        self.threeD_lat.rand_metro_relax(iterations)
        self.config = self.threeD_lat.lattice_values
        
    def visualize(self):
        '''
        Plot the configuration of the Ising Lattice after thermalization
        '''
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111, projection='3d')
        pos = np.where(self.config==1)
        ax1.scatter(pos[0], pos[1], pos[2], c='black')
        pos1 = np.where(self.config==-1)
        ax1.scatter(pos1[0], pos1[1], pos1[2], c='red')
        plt.show()
        

def vis(temperature, size, field, start, iterations):
    '''
    The main function of this package, plot a 3D Ising Lattice's Initial and 
    Final Configurations
    '''
    visual = ThreeD_Vis(temperature, size, field,0 ,start)
    visual.visualize()
    plt.title('Initial Configuration')
    visual = ThreeD_Vis(temperature, size, field,iterations ,start)
    visual.visualize()
    plt.title('Final Configuration')
        
def main():
    '''
    Just a testing Function Wrapped in main()
    '''
    vis(0, 3, 0, 'random', 100000)
    

if __name__ == '__main__':
    main()

        
        
        

