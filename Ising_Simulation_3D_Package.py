#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''
#import matplotlib.pyplot as plt #matplotlib isn't working on my home server, wouldn't matter any, since no graphical output
import os
import emailer #import my own emailing package
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import numpy as np
from random import randrange, random, choice
import time
import os.path  


class Ising_Lattice_3D:
    '''
    This is a class for simulating 3 Dimensional Ising Lattices. It is currently reasonably slow
    and has yet to be properly optimized. The lattices are cubic, created with an
    nxnxn sizes, and at an initial temperature and spin configuration, with an fixed
    background magnetic field, if desired. The lattice can be thermalized and measured.
    '''
    def __init__(self, size, temp, field, init_spins):
        self.size = size #cube root of the number of spins in the lattice
        self.temp = temp #Initial temperature of the lattice
        self.field = field #Background field in normalized units
        self.init_spins = init_spins 
        if self.init_spins == 'random': #Set the spins all up or randomly, as desired
            self.lattice_values = self.mk_rand()
        elif self.init_spins == 'ups':
            self.lattice_values = self.mk_up()
        self.curr_mag = np.float64(0) #Keep track of the current magnitization of the lattice
        self.curr_energy = np.float64(0) #And the current energy of the lattice
        self.det_energy() #Initialize the energy and magnitization of the lattice
        self.det_mag()
        self.t_ave_e = np.float64(0)
        self.t_ave_m = np.float64(0)
        self.mlist = [] #This list keeps track of all the magnitization changes we make when flipping spins while measuring
        self.elist = [] #same as above, but for energies
        self.mag_sus = np.float64(0) #Store the average magnetic sus
        self.heat_cap = np.float64(0) # And the average heat capacity
        
    def det_energy(self):
        '''
        This method sweeps the entire 3D lattice, and determins the total energy of the lattice
        '''
        energy = np.float64(0) #Start counting the energy
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                for z in range(self.size):
                    energy = energy + self.E_xyz(x, y, z) #Determine the energy at each spin
        self.curr_energy = energy / 2. #To prevent double counting
        
    def det_mag(self):
        '''
        This method sweeps the entire lattice, and determines the total magnitization of the
        lattice in its current configuration
        '''
        mag = np.float64(0) #Start counting the magnitization
        for x in range(self.size): #Loop through all the spins in the lattice
            for y in range(self.size):
                for z in range(self.size):
                    mag = mag + self.lattice_values[x, y, z]
        self.curr_mag = mag 
        
    def set_temp(self, new_temp):
        '''
        This method sets the temperature of the lattice to the new desired 
        tempeature, new_temp
        '''
        self.temp = new_temp
        
    def mk_rand(self): #Make an array of random spins
        '''
        This method initializes the lattice with a random configuration of spins
        '''
        lat = np.zeros((self.size, self.size, self.size), dtype=int) # sizexsizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                for z in range(self.size): #Loop over the z values
                    lat[x, y, z] = choice([1, -1]) #Randomly assign spins
        return lat
    def mk_up(self): #Make an array of up spins
        '''
        This method initializes the lattice with a homogenous configuration of spins,
        all spins up
        '''
        lat = np.zeros((self.size, self.size, self.size), dtype=int) # sizexsizexsize array of zeros
        for x in range(self.size): #Loop over x values
            for y in range(self.size): #Loop over y values
                for z in range(self.size):
                    lat[x, y, z] = 1 #set all spins to 1
        return lat
    def E_xyz(self, x, y, z):
        '''
        This method determines the energy of a single spin in the lattice,
        by looking at the states of all six neighbouring spins, 
        using periodic boundary condition.
        '''
        if self.field==0:
            return (-1.0 * self.lattice_values[x, y, z]*
                    (self.lattice_values[(x + 1) % self.size, y, z] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y, z] +
                     self.lattice_values[x, (y + 1) % self.size, z] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size, z] +
                     self.lattice_values[x, y, (z + 1) % self.size] +
                     self.lattice_values[x, y, (z - 1 +self.size) % self.size]))
        else: #If there is a magnetic field, energy will also depend on field strenght
            return (-1.0 * self.lattice_values[x, y, z]*
                    (self.lattice_values[(x + 1) % self.size, y, z] +
                     self.lattice_values[(x - 1 + self.size) % self.size, y, z] +
                     self.lattice_values[x, (y + 1) % self.size, z] +
                     self.lattice_values[x, (y - 1 + self.size) % self.size, z] +
                     self.lattice_values[x, y, (z + 1) % self.size] +
                     self.lattice_values[x, y, (z - 1 +self.size) % self.size]) +
                     self.lattice_values[x, y, z]*self.field)

    def rand_metro_relax(self, nsteps): 
        '''
        This method thermalizes the lattice over a number of iterations
        nsteps, and does not take any measurements while doing so
        Once thermalization is complete, the energy and magnitization of the chain
        are updated
        '''
        for i in range(int(nsteps)): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            z = randrange(self.size)
            #Work out the change in energy for flipping the atom's spin, new energy minus old energy
            E_delta = - 2 * self.E_xyz(x, y, z) #Flip the spin (*-1) and subract original energy
            #Checking if the energy is tending to a minimum
            if E_delta <= 0.: #Is the energy change negative?
                self.lattice_values[x, y, z] = self.lattice_values[x, y, z] * -1 #Flip the spin
            #Does a boltzmann velocity distro allow the flip anyway?
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y, z] = self.lattice_values[x, y, z] * -1 #Flip the spin
                #Otherwise, keep the spin the same
        self.det_mag() #Update the magnitization of this relaxed configuration
        self.det_energy() #Update to the energy of this relaxed configuration
    def rand_metro_measure(self, nsteps):
        '''
        This method determines the average energy, magnetization,
        heat capacity and magnetic susceptability per spin on the chain by performing
        the metropolis algorithm nsteps times.
        '''
        for i in range(int(nsteps)): #Do this for as many iterations as requested
            x = randrange(self.size) #Pick an atom
            y = randrange(self.size)
            z = randrange(self.size)
            flip = False
            E_delta = - 2 * self.E_xyz(x, y, z)
            if E_delta <= 0.:
                self.lattice_values[x, y, z] = self.lattice_values[x, y, z] * -1 #Flip the spin
                flip = True
            elif random() < np.exp(-1. * E_delta/(self.temp)):
                self.lattice_values[x, y, z] = self.lattice_values[x, y, z] * -1 #Flip the spin
                flip = True #We flipped it
                
            if flip: #If we flipped, need to update energies and mags
                self.curr_energy += E_delta / 2. #To prevent double counting in the long run
                self.curr_mag += self.lattice_values[x, y, z]
                self.mlist.append(self.curr_mag)
                self.elist.append(self.curr_energy)
        self.t_ave_e = self.curr_energy / self.size**3 #Need to divide by n^3 this time
        self.t_ave_m = self.curr_mag / self.size**3
        ave_e = np.average(self.elist)
        ave_m = np.average(self.mlist)
        ave_e_sq = ave_e**2
        ave_m_sq = ave_m**2
        try: #If we dont flip any spins, will get a divide by zero
            ave_m_to_be_squared = sum(np.array(self.mlist)**2)/len(self.mlist)
        except ZeroDivisionError: #Catch the divide by zero, and set the value to zero
            #So the that mag sus will be zero
            ave_m_to_be_squared = np.float64(0)
        try:
            ave_e_to_be_squared = sum(np.array(self.elist)**2)/len(self.elist)
        except ZeroDivisionError: #Same as before
            ave_e_to_be_squared = np.float64(0)
        self.mag_sus = (self.size**(-3)*(1.0/self.temp))*(ave_m_to_be_squared-ave_m_sq)
        self.heat_cap = (self.size**(-3)*(1.0/self.temp)**2)*(ave_e_to_be_squared-ave_e_sq)
        '''
        Magnetization = <M>/#spins
        energy = <E>/spins
        heat capacities = 1/T^2 <E^2> - <E>^2
        susceptibilities = 1/T <M^2> - <M>^2
        '''
        
def Ising_Simulation_3D(temps, iterations, meaning_iterations, size):
    '''
    This helper function allows the 3D ising model simulations to be more
    easily carried out
    '''
    mean_energy = [] #The average energies from all meaning_iteration
    mean_magnet = [] #The average magntiization for the same
    mean_heat_cap = [] #Ditto
    mean_magnetic_sus = [] #Same again
    for j in range(meaning_iterations):
        #For each meaning iteration, we do
        temps = temps
        iterations = round(iterations) #Make sure its iterable
        measure_iterations = round(iterations/2) #Same again
        ave_magnetizations = [] #Keep track of the magnitizations for these temperatures
        ave_energies = [] #And the energies
        magnetic_susceptibilities = [] #And heatcaps
        heat_capacities = [] #And susceptibilities
        lattice1 = Ising_Lattice_3D(size, temps[0], 0, 'ups') #Make lattice!
        for i in temps: #for each temperature
            lattice1.set_temp(i) #Set the lattice to the new temperature
            lattice1.rand_metro_relax(iterations) #Thermalize it
            lattice1.rand_metro_measure(measure_iterations) #measure it
            #print(lattice1.lattice_values)
           
            ave_energies.append((lattice1.t_ave_e))
            ave_magnetizations.append(abs(lattice1.t_ave_m))
            heat_capacities.append(lattice1.heat_cap)
            magnetic_susceptibilities.append(lattice1.mag_sus)
            #print('Simulation For Temperature ',i,' Complete') #debugging
        mean_energy.append(ave_energies)
        mean_magnet.append(ave_magnetizations)
        mean_heat_cap.append(heat_capacities)
        mean_magnetic_sus.append(magnetic_susceptibilities)
        print('Simulation Complete for Iteration', j,' of ',meaning_iterations) #Debugging/benchmarking
    #Average the measuremables at each temperature over all the meaning iterations
    mean_energy = np.array(np.mean(mean_energy, axis = 0))
    mean_magnet = np.array(np.mean(mean_magnet, axis = 0))
    mean_heat_cap = np.array(np.mean(mean_heat_cap, axis = 0))
    mean_magnetic_sus = np.array(np.mean(mean_magnetic_sus, axis = 0))
    #And return those meaned values as output
    return temps, mean_energy, mean_magnet, mean_heat_cap, mean_magnetic_sus


def main():
    '''
    A test of this package
    '''
    temps = np.linspace(3,5, 40) #Use the same temperature points
    #Run the simulation
    temps, mean_energy, mean_magnet, mean_heat_cap, mean_magnetic_sus = Ising_Simulation_3D(temps, 1e2, 10, 5)
    #Writing the output to disk
    filename = 'First3DIsingMmodelTest.txt'
    print('Writing ASCII File')
    lattice_data = Table([temps,mean_energy,mean_magnet,mean_heat_cap,mean_magnetic_sus], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
    ascii.write(lattice_data, filename)
    #self emailing details
    my_email = 'alexjanhackett@gmail.com'
    my_passw = 'Portumna1' #TODO, remove before submission
    recipients = ['alexjanhackett@gmail.com']
    subject = 'First 3D Ising Model Test'
    message = 'This is the output of a 10x10x10 lattice, thermalized 1e5 iterations, measured 5e4 iterations, each meaned 5 times, over a temperature range from 1.5 to 3.5 with 40 steps. I am reciving this email and file because I ran the 3D Simulation Package as a programme rather than importing it! Hopefully the data is still of some use!! '
    file_name = filename
    #Emailing the disk file to myself!
    emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)
    '''
    Debugging, mpl doesn't run on my home server anyway
    '''
#    energy1 = plt.figure()
#    plt.plot(temps, mean_energy)
#    plt.title('Mean Energy With Temperature')
#    mag1 = plt.figure()
#    plt.plot(temps, mean_magnet)
#    plt.title('Mean Magentization w/ Temp')
#    heatcap1 = plt.figure()
#    plt.plot(temps, mean_heat_cap)
#    plt.title('Mean Heat Cap w/ temp')
#    magsus1 = plt.figure()
#    plt.plot(temps, mean_magnetic_sus)
#    plt.title('Mean mag sus w/ temp')
if __name__ == '__main__':
    main()
        
