#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This simple script was utilized to test for convergence of the Ising
Model for a large lattice size at the analytical critical temperature
'''

import Ising_Simulation_Package as ISP
import numpy as np
import astropy as ap
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import emailer
import matplotlib as mpl
mpl.use('Qt5Agg')
import matplotlib.pyplot as plt


#Defining function
def thermalization_test(sweeps, lattice, increment):
    '''
    This function takes an array of sweeps, a number of times to sweep through 
    a lattice, the lattice object itself, and the increment between the steps
    in the sweep array. The function returns a list of the magnetization of the 
    lattice as measured after each thermalizing sweep
    '''
    ave_mag = []
    i = 0
    while i <= sweeps[-1]: #For all the sweeos
        lattice.rand_metro_relax((lattice.size**2) * increment) #Sweep through
        #The lattice that many times
        ave_mag.append(abs(lattice.curr_mag) / (lattice.size**2))#Record the 
        #magnetization per spin
        #print('Sweep Complete for Sweep ',int(i)) #Debugging/benchmarking
        i += increment #INcrement the counter
        
    return ave_mag #Return the list of magnetizations

#Wrapping everything in main()
def main(size, temp):
    increment = 1 #Set the increment
    sweep_array = np.arange(0,1500,increment) #and create the array
    mean_hot = [] #List to store the mean values for the randomly initizlied lattice
    mean_cold = [] #And for the aligned lattice
    meaning_iterations = 20 #Do this 20 times, and take the average
    for i in range(meaning_iterations):
        #Create the lattices
        converge_lattice_hot = ISP.ising_lattices(size, temp, 0, 'random')
        converge_lattice_cold = ISP.ising_lattices(size, temp, 0, 'ups')
        #Determining the converge of the lattices
        hot_start_mag = thermalization_test(sweep_array, converge_lattice_hot,increment)
        cold_start_mag = thermalization_test(sweep_array, converge_lattice_cold,increment)
        #Appending values from this iteration to the list
        mean_hot.append(hot_start_mag)
        mean_cold.append(cold_start_mag)
        print('Meaning Iteration ',i, ' Complete')
        
    #average all the values for each sweep to reduce statistical noise
    mean_hot = np.array(np.mean(mean_hot, axis = 0))
    mean_cold = np.array(np.mean(mean_cold, axis = 0))
    
    #Performing the plotting
    fig1 = plt.figure()
    plt.plot(sweep_array, mean_cold, '-b', label = 'Cold Start')
    plt.plot(sweep_array, mean_hot, '-r', label = 'Hot Start')
    plt.xlabel('Number of Sweeps')
    plt.ylabel('Order Parameter (Magnetization)')
    plt.title('Determination of Convergence for 50x50 Lattice')
    plt.legend()
    
    #Writing the data to disk for future analysis if required
    filename = 'convergence.txt'
    print('Writing ASCII File')
    lattice_data = Table([sweep_array,mean_hot,mean_cold], names=['sweeps', 'hot_start_magnetization','cold_start_magnetization'])
    ascii.write(lattice_data, filename)
    #self emailing details
    my_email = 'alexjanhackett@gmail.com'
    my_passw = ''
    recipients = ['alexjanhackett@gmail.com']
    subject = 'FinalConvergence Test'
    message = 'Final Convergence test'
    file_name = filename
    #Emailing the disk file to myself!
    emailer.email_me(my_email, my_passw, recipients, subject, message, file_name)
    
if __name__ == '__main__':
    main(50,2.3)
    
