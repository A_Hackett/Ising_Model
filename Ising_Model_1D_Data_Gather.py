#!/bin/env python


''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This is a script written to take measurments using the 1D Ising Simulation P
Package. 

The Script runs the Ising Simulation for a 100 spin chain, the Primary
Lattice Size Investigated, for an appropriate number of iterations and
meaning iterations to produce statistically relevent results

In addition to saving these results to a file, the
script produces a set of 4 Rudimentory plots using matplotlib in order
to briefly visualise the solution, before importing the data
into a data analysis tool such as Origin

WARNING! In its current configuration, this script
has an excessivly long runtime, although it is nowhere near as bad as the 2 or 
3 D versions of the model, due to being significantly simpler
'''

#Importing Required Packages
import Ising_Simulation_1D_Package as IS
import numpy as np
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt



#Setting the values used to produce the 3D Dataset
temp = np.linspace(0,10,100) #These cover a wider range than in the two or 3 D Case,
#Since there is no phase transition expected in the one D case except at 
#zero temperature
size = 100
iterations = 1e5
meaning_iterations = 10

#Wrapping everything in main
def main(temp,size,iterations, meaning_iterations):
    #Perform the Ising Model Simulation
    T, E, M, HOT, SUS, = IS.ONED_ISING_SIM(temp, iterations,
                                                   meaning_iterations, size)
    print('Simulation Complete')
    #Writing the Data to file
    filename = '1D_Ising_Lattice_Data.txt'
    print('Writing ASCII File')
    lattice_data = Table([T,E,M,HOT,SUS], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
    ascii.write(lattice_data, filename)
    print('Data Written To Disk')
    print('Plotting')
    #Plotting the data
    fig1 = plt.figure()
    plt.plot(T,E)
    plt.title('Plot of Average Energy against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Energy per Spin')
    
    fig2 = plt.figure()
    plt.plot(T,M)
    plt.title('Plot of Average Magnetization against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Magnetization per Spin')
    
    fig3 = plt.figure()
    plt.plot(T,HOT)
    plt.title('Plot of Average Heat Capacity against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Heat Capacity per Spin')
    
    fig4 = plt.figure()
    plt.plot(T,SUS)
    plt.title('Plot of Average Magnetic Susceptability against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Magnetic Susceptability per Spin')
    
    
#Run the script if executed Directly
if __name__ == '__main__':
    main(temp,size,iterations, meaning_iterations)
    
    
