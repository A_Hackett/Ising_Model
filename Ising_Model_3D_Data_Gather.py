#!/bin/env python

''' 
 Author: Alexander Hackett 
 Student Number  15323791 
 ahackett@tcd.ie 
 Created for Prof. Archers assignment, Ising Model Simulation 
'''

'''
This is a script written to take measurments using the 3D Ising Simulation P
Package. 

The Script runs the Ising Simulation for a 10x10x10 Lattice, the Primary
Lattice Size Investigated, for an appropriate number of iterations and
meaning iterations to produce statistically relevent results

In addition to saving these results to a file, the
script produces a set of 4 Rudimentory plots using matplotlib in order
to briefly visualise the solution, before importing the data
into a data analysis tool such as Origin

WARNING! In its current configuration, this script
has an excessivly long runtime
'''

#Importing Required Packages
import Ising_Simulation_3D_Package as IS
import numpy as np
from astropy.table import Table, Column, MaskedColumn
from astropy.io import ascii
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt



#Setting the values used to produce the 3D Dataset
temp = np.linspace(0,8,50) #These cover a wider range than in the two D Case,
#Since The phase transition in the 3D case has not been characterized
#Analytically, from literature, expected to be ~4.5
size = 10
iterations = 1e5
meaning_iterations = 60

#Wrapping everything in main
def main(temp,size,iterations, meaning_iterations):
    #Perform the Ising Model Simulation
    T, E, M, HOT, SUS, = IS.Ising_Simulation_3D(temp, iterations,
                                                   meaning_iterations, size)
    print('Simulation Complete')
    #Writing the Data to file
    filename = '3D_Ising_Lattice_Data.txt'
    print('Writing ASCII File')
    lattice_data = Table([T,E,M,HOT,SUS], names=['Temperature', 'Average_Energy','Average_Magnetization', 'Heat_Capacity', 'Magnetic_Susceptibility' ])
    ascii.write(lattice_data, filename)
    print('Data Written To Disk')
    print('Plotting')
    #Plotting the data
    fig1 = plt.figure()
    plt.plot(T,E)
    plt.title('Plot of Average Energy against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Energy per Spin')
    
    fig2 = plt.figure()
    plt.plot(T,M)
    plt.title('Plot of Average Magnetization against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Magnetization per Spin')
    
    fig3 = plt.figure()
    plt.plot(T,HOT)
    plt.title('Plot of Average Heat Capacity against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Heat Capacity per Spin')
    
    fig4 = plt.figure()
    plt.plot(T,SUS)
    plt.title('Plot of Average Magnetic Susceptability against Temperature')
    plt.xlabel('Normalized Temperature')
    plt.ylabel('Average Magnetic Susceptability per Spin')
    
    
#Run the script if executed Directly
if __name__ == '__main__':
    main(temp,size,iterations, meaning_iterations)
    
    
